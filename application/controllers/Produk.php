<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//require_once(APPPATH . 'modules/auth/controllers/Base_global.php');

class Produk extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('web_model'));
        $this->load->helper('captcha');

        $this->modul = $this->uri->segment(1);
     
    }

     public function index() {

            $j = $this->input->get('j');
            $keyword = $this->input->get('key');

            if ($j == 'k') {
                $jenis_data = 'kategori';
                $data['detail'] = $this->web_model->view('kategori',array('slug'=>$keyword));
            } elseif ($j == 'm') {
                $jenis_data = 'merk';
                $data['detail'] = $this->web_model->view('merk',array('slug'=>$keyword));
            } else {
                $jenis_data = false;
                $keyword = false;
            }

            $jumlah_produk = $this->web_model->get_jumlah_produk($jenis_data,$keyword);

            // echo $this->db->last_query(); die();


            if ($jumlah_produk == 0) {
                $data['text_judul'] = 'Produk tidak ditemukan';
            } elseif ($j == 'k') {
                $data['text_judul'] = 'Ditemukan '.$jumlah_produk.' Produk Dengan Kategori '.$data['detail'][0]->nama_kategori;
            } elseif ($j == 'm') {
               $data['text_judul'] = 'Ditemukan '.$jumlah_produk.' Produk Dengan Merk '.$data['detail'][0]->nama_merk;
            } else {
                $data['text_judul'] = 'Ditemukan '.$jumlah_produk.' Produk';
            }

            // echo $jumlah_produk; die();

            $config['base_url'] = base_url().'produk/index/';
            $config['total_rows'] =  $jumlah_produk;
            $config['per_page'] = 3;
            $config['use_page_numbers'] = TRUE;
            // $config['page_query_string'] = TRUE;

            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="page-item disabled"><a class="page-link" href="#">';
            $config['cur_tag_close'] = '</a></li>';

            // $config['anchor_class'] = 'class="page-link"';
            //$config['anchor_class'] = 'class="page-link" ';

            $config['attributes'] = array('class' => 'page-link');
            $config['reuse_query_string'] = true;

            $this->pagination->initialize($config);

            $p = $this->uri->segment(3,0);
            $offset = ($p>1) ? ($p * $config['per_page']) - $config['per_page'] : 0;

            $data['pagination'] = $this->pagination->create_links();

            $data['produk'] = $this->web_model->ambil_produk($jenis_data,$config['per_page'],$offset,$keyword);
            $data['merk'] =  $this->web_model->ambil_merk();

            $data['jumlah_produk'] = $jumlah_produk;

            $data['p'] = 'web/v_semua_produk';
            $this->load->view('template',$data);
       
    }

    public function kategori($slug) {
            if ($slug == false) redirect('produk');

            $jumlah_produk = $this->web_model->get_jumlah_produk('kategori',$slug);

            // echo $this->db->last_query(); die();
            $config['base_url'] = base_url('kategori/'.$slug);
            $config['total_rows'] =  $jumlah_produk;
            $config['per_page'] = 3;
            $config['use_page_numbers'] = TRUE;
            // $config['page_query_string'] = TRUE;

            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="page-item disabled"><a class="page-link" href="#">';
            $config['cur_tag_close'] = '</a></li>';

            // $config['anchor_class'] = 'class="page-link"';
            //$config['anchor_class'] = 'class="page-link" ';

            $config['attributes'] = array('class' => 'page-link');

            $this->pagination->initialize($config);

            $p = $this->uri->segment(3,0);
            $offset = ($p>1) ? ($p * $config['per_page']) - $config['per_page'] : 0;

            $data['pagination'] = $this->pagination->create_links();

            $data['produk'] = $this->web_model->ambil_produk('kategori',$config['per_page'],$offset,$slug);
            $data['merk'] =  $this->web_model->ambil_merk();


            $data['jumlah_produk'] = $jumlah_produk;
            $data['text_judul'] = 'Dengan Kategori '.$slug;

            $data['p'] = 'web/v_semua_produk';
            $this->load->view('template',$data);
       
    }

    public function detail($slug = false) {
        if ($slug==false) { redirect($this->modul); }
        $data['detail'] = $this->web_model->detail_produk($slug);
        if (empty($data['detail'])) { redirect($this->modul); }

        $id_produk = $data['detail']->id;
        $id_kategori = $data['detail']->id_kategori;
        $id_merk = $data['detail']->id_merk;

        $data['gambar'] = $this->web_model->view('gambar',array('id_produk'=>$id_produk));
        $data['produk_sejenis'] = $this->web_model->ambil_produk_sejenis($id_produk,$id_kategori,$id_merk);

        $data['p'] = 'web/v_detail_produk';
		$this->load->view('template',$data);
    }


    public function keranjang_belanja($id = false) {
        $data['judul'] = 'Keranjang Belanja';

        $data['p'] = 'web/v_keranjang_belanja';
        $this->load->view('template',$data);
    }




    public function tambah_ke_keranjang($slug = false) {
        $data['detail'] = $this->web_model->detail_produk($slug);

        // Add product to the cart
        $namaproduk = cleanstring($data['detail']->nama_produk);
        $data = array(
            'id'    => $data['detail']->id,
            'qty'    => 1,
            'price'    => $data['detail']->harga,
            'name'    => $namaproduk,
            'image' => $data['detail']->gambar_utama
        );

        $this->cart->insert($data);
        $this->session->set_flashdata('sukses', 'Berhasil menambahkan ke keranjang belanja.');

        // Redirect to the cart page
        redirect('produk/keranjang_belanja');
    }

    public function edit_keranjang() {
        $rowid = $this->input->post('rowid');

        $data=$this->cart->update(array(
            'rowid'=>$rowid,
            'qty'=> $this->input->post('qty')
        ));

        $this->cart->update($data);  

        redirect('produk/keranjang_belanja');

    }

    function buat_captcha() {
        $vals = array(
        'img_path'      => './captcha/',
        'img_url'       => base_url('captcha'),
        // 'font_path'     => './path/to/fonts/texb.ttf',
        'img_width'     => '150',
        'img_height'    => 40,
        'expiration'    => 7200,
        'word_length'   => 3,
        // 'font_size'     => 12,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );
        $cap = create_captcha($vals);

        return $cap;
    }

    public function checkout () {
        if ($this->cart->total() == 0 ) {
            redirect('produk/keranjang_belanja');
        }

        $data['data_member'] = $this->web_model->view('pengguna',array('username'=>$this->session->userdata('username')));

        $data['cap'] = $this->buat_captcha();


        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$data['cap']['word']);

        $data['p'] = 'web/v_checkout';
        $this->load->view('template',$data);   
    }

    public function checkout_proses () {
            $post = $this->input->post();
            // print_r_pre($post);
            if (!empty($post)) {
                $inputCaptcha = $this->input->post('captcha');
                $sessCaptcha = $this->session->userdata('captchaCode');

            if ($inputCaptcha != $sessCaptcha) {
                echo '<div align="center">';
                echo 'Captcha Salah..<br>';
                echo '<a href="javascript:history.back()">Ulangi</a>';
                echo '</div>';
                die();  
            }

            //validasi
            $this->form_validation->set_rules('nama_penerima', 'Nama Penerima', 'required');
            $this->form_validation->set_rules('no_hp_penerima', 'Nomor HP', 'required|max_length[20]');
            $this->form_validation->set_rules('alamat_pengiriman', 'Alamat', 'required');

            if ($this->form_validation->run() == FALSE) {
                echo '<div align="center">';
                echo validation_errors().'<br>';
                echo '<a href="javascript:history.back()">Ulangi</a>';
                echo '</div>';
                die();
            } else {
                $id_max = $this->web_model->ambil_id_max('transaksi');
                $id_baru = $id_max + 1;
                $nomor_transaksi = date('Ymd').'-'.$id_baru;

                $nama_penerima = $post['nama_penerima'];
                $no_hp_penerima = $post['no_hp_penerima'];
                $alamat_pengiriman = $post['alamat_pengiriman'];

                $datatransaksi = array(
                            'nomor_transaksi'=>$nomor_transaksi,
                            'username'=> $this->session->userdata('username'),
                            'total'=> $this->cart->total(),
                            'status'=>'baru',
                            'nama_penerima'=> $nama_penerima,
                            'no_hp_penerima'=>$no_hp_penerima,
                            'alamat_pengiriman'=>$alamat_pengiriman    
                            );
                $i=0;
                foreach ($this->cart->contents() as $key => $item) {
                    $datadetail[$i]['id_transaksi'] = $id_baru;
                    $datadetail[$i]['id_produk'] = $item['id'];
                    $datadetail[$i]['jumlah_beli']= $item['qty'];
                    $datadetail[$i]['harga'] = $item['price']; 
                    $i++; 
                }



                $this->web_model->insert('transaksi',$datatransaksi);
                // print_r_pre($datadetail); die();
                $this->db->insert_batch('transaksi_detail', $datadetail);

                $this->cart->destroy();

                $this->session->set_flashdata('sukses','Checkout Berhasil.');

                redirect ('member/transaksi');   
            }

            
        } else {
            redirect('produk/checkout');
        }

    }



    function hapus_keranjang(){
        $rowid = $this->input->post('id');

        //echo $rowid; die();
        // Remove item from cart
        $remove = $this->cart->remove($rowid);
        
        // Redirect to the cart page
        redirect('produk/keranjang_belanja/');
    }



    

    public function cart($id = false) {
        // print_r_pre($this->cart->contents()); die();
        $data['category'] = $this->Product_m->get_category(0,100);

        $data['judul'] = 'Keranjang Belanja';

        $data['content'] = 'product/v_cart';
        $this->load->view('template',$data);
    }




    public function add_to_cart($id = false) {
        $data['detail'] = $this->Product_m->detail($id);

        $namaproduk = cleanstring($data['detail']->product_name);
        $data = array(
            'id'    => $data['detail']->id,
            'qty'    => 1,
            'price'    => $data['detail']->price,
            'name'    => $namaproduk,
            'image' => $data['detail']->image_ori
        );

        $this->cart->insert($data);
        redirect('keranjang-belanja');
    }

    public function update_cart($id = false) {

        $this->cart->update($this->input->post());
         redirect('product/cart/');

        $id = dekripsi($id);
        
        // Fetch specific product by ID
        $this->data['detail'] = $this->Product_m->detail($id);
        
        // Add product to the cart
        $data = array(
            'id'    => $this->data['detail']->id,
            'qty'    => 1,
            'price'    => $this->data['detail']->price,
            'name'    => $this->data['detail']->product_name,
            'image' => $this->data['detail']->image_ori
        );
        $this->cart->insert($data);
        
        // Redirect to the cart page
        redirect('product/cart/');
    }

    function removeItem($rowid){
        // Remove item from cart
        $remove = $this->cart->remove($rowid);
        
        // Redirect to the cart page
        redirect('product/cart/');
    }

    function create_captcha() {
        $vals = array(
        'img_path'      => './captcha/',
        'img_url'       => base_url('captcha'),
        // 'font_path'     => './path/to/fonts/texb.ttf',
        'img_width'     => '150',
        'img_height'    => 40,
        'expiration'    => 7200,
        'word_length'   => 3,
        // 'font_size'     => 12,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );
        $cap = create_captcha($vals);

        return $cap;
    }

    public function refresh_captcha(){
        $data['cap'] = $this->create_captcha();

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$data['cap']['word']);
        
        // Display captcha image
        echo $data['cap']['image'];
    }

    public function checkouthh() {
        // print_r_pre($this->cart->contents()); die();

        $data['cap'] = $this->create_captcha();

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$data['cap']['word']);

        // $cap = create_captcha($vals);
        // print_r($this->data['cap']); die();
        $data['judul'] = 'Checkout';
        $data['content'] = 'product/v_checkout';
        $this->load->view('template',$data);
    }

   


    public function thanks() {
        $data['content'] = 'v_thanks';
        $this->load->view('template',$data);
    }


   
    

    public function cari() {

        $q = cleanstring($this->input->get('q'));
        //$q = 'Jilbab';
        $data['q'] = $q;
        //echo $q; die();

        $jumlah_produk = $this->Product_m->get_jumlah_produk($q);

        $config['base_url'] = base_url().'product/cari/';
        $config['total_rows'] =  $jumlah_produk;
        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = true;
        // $config['page_query_string'] = TRUE;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';

       

        $this->pagination->initialize($config);

        $p = $this->uri->segment(3,0);
        $offset = ($p>1) ? ($p * $config['per_page']) - $config['per_page'] : 0;

        $data['pagination'] = $this->pagination->create_links();

        $data['produk'] = $this->Product_m->get_product($offset,$config['per_page'],$q);

        // echo $jumlah_produk;
        // print_r_pre ($data['produk']); die();

        // echo $this->db->last_query(); die();
        $data['judul'] = 'Jual '.$q;

        $data['content'] = $this->modul.'/v_search';
        $this->load->view('template',$data);


    }


}
