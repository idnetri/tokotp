<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//require_once(APPPATH . 'modules/auth/controllers/Base_global.php');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('web_model'));
        $this->load->helper('captcha');
        $this->username = $this->session->userdata('username');

        $this->modul = $this->uri->segment(1);
     
    }


    function index () {
    	if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['pengguna'] = $this->web_model->view('pengguna',array('username'=>$this->username));
        $data['transaksi'] = $this->web_model->ambil_transaksi('member',$this->username);

        // print_r_pre($data['pengguna']); 
        $data['p'] = 'admin/v_admin';
        $this->load->view('templateadmin',$data);
    }

    function buat_captcha() {
        $vals = array(
        'img_path'      => './captcha/',
        'img_url'       => base_url('captcha'),
        // 'font_path'     => './path/to/fonts/texb.ttf',
        'img_width'     => '150',
        'img_height'    => 40,
        'expiration'    => 7200,
        'word_length'   => 3,
        // 'font_size'     => 12,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );
        $cap = create_captcha($vals);

        return $cap;
    }

    function daftar () {
        // $data['pengguna'] = $this->web_model->view('pengguna',array('username'=>$this->username));
        // $data['transaksi'] = $this->web_model->ambil_transaksi('member',$this->username);

        // print_r_pre($data['pengguna']); 

        $data['cap'] = $this->buat_captcha();

        // print_r($data['cap']); die();

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$data['cap']['word']);

        $data['p'] = 'member/v_daftar';
        $this->load->view('template',$data);
    }

    function daftar_proses () {
        // redirect('member/daftar'); 
        // print_r_pre($this->session->userdata()); die();
        $post = $this->input->post();
        // print_r_pre($post); die();
        if (!empty($post)) {
            $inputCaptcha = $this->input->post('captcha');
            $sessCaptcha = $this->session->userdata('captchaCode');

            // print_r_pre($this->session->all_userdata()); die();

            if ($post['pass1'] != $post['pass2']) {
                echo '<div align="center">';
                echo 'Konfirmasi Password Salah..<br>';
                echo '<a href="javascript:history.back()">Ulangi</a>';
                echo '</div>';
                die();  
            }

            //validasi
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[50]|min_length[5]');
            $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]');
            $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|max_length[255]');
            $this->form_validation->set_rules('no_hp', 'Nomor HP', 'required|max_length[20]');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[30]');
            $this->form_validation->set_rules('pass1', 'Password', 'required|min_length[6]|max_length[255]');
            $this->form_validation->set_rules('pass2', 'Konfirmasi Password', 'required|min_length[6]|max_length[255]');

            if ($this->form_validation->run() == FALSE) {
                echo '<div align="center">';
                echo validation_errors().'<br>';
                echo '<a href="javascript:history.back()">Ulangi</a>';
                echo '</div>';
                die();
            } else {

                $datausername = $this->web_model->view('pengguna',array('username'=>$post['username']));

                //print_r_pre($datausername); die();

                if ($datausername[0]->username != '') {
                    echo '<div align="center">';
                    echo 'Username tidak tersedia!<br>';
                    echo '<a href="javascript:history.back()">Ulangi</a>';
                    echo '</div>';
                    die(); 
                }

                //$id_max = $this->web_model->ambil_id_max('transaksi');
                //$id_baru = $id_max + 1;
                //$nomor_transaksi = date('Ymd').'-'.$id_baru;


                $password = md5($post['pass1']);

                $datainput = array(
                            'username'=> $post['username'],
                            'password'=> $password,
                            'nama_lengkap'=> $post['nama_lengkap'],
                            'jenis_kelamin'=> $post['jk'],
                            'no_hp'=> $post['no_hp'],
                            'alamat_lengkap'=>$post['alamat'],
                            'email'=> $post['email'],
                            'role' => 2,
                            'is_divalidasi'=> 1    
                            );

                $this->web_model->insert('pengguna',$datainput);

                $this->session->set_flashdata('sukses','Pendaftaran Berhasil. Silakan Login');

                redirect ('member/login');   
            }

            
        } else {
            redirect('member/daftar');
        }


    }

    public function login() {

        $post = $this->input->post();

        $data['aaa'] = '';
        // print_r_pre($post);
        if (!empty($post)) {
            $ceklogin = $this->web_model->cekLogin($post['username'],$post['pass'],'admin');
           	//echo $this->db->last_query(); die();

            if ($ceklogin == null) {

                $back = "username";
                $msg = "Username atau password yang Anda input salah.";
                // $this->Login_model->update_failed_login($this->input->post('username'));
                // echo json_encode(array('back' => $back, 'msg' => $msg));

                redirect('admin/login');
            } else {
                $datasession = array(
                    "logged_in" => TRUE,
                    "username" => $ceklogin[0]->username,
                    "nama_lengkap" => $ceklogin[0]->nama_lengkap,
                    "role" => $ceklogin[0]->role,
                    "is_aktif" => $ceklogin[0]->is_aktif
                );
                $this->session->set_userdata($datasession);
                
                $this->session->set_flashdata('sukses','Login Berhasil.');
                redirect('admin');
               

            }
            
        } else {
            $this->load->view('admin/v_login',$data);
        }
       
    }

    public function logout () {
        session_destroy();

        $this->session->set_flashdata('sukses','Anda Berhasil Logout.');

        redirect('admin/login');
    }

    public function merk() {
    	if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Data Merk';
        $data['datanya'] = $this->web_model->ambil_merk('desc');
    	$data['p'] = 'admin/v_list_merk';
    	$this->load->view('templateadmin',$data);
    }

    public function tambah_merk() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['jenis'] = 'Tambah';
        $data['judul'] = 'Tambah Merk';
        $data['aksi'] = 'admin/tambah_merk_proses';
        $data['p'] = 'admin/v_form_merk';
        $this->load->view('templateadmin',$data);
    }

    public function tambah_merk_proses() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        
        $data['aksi'] = 'admin/tambah_merk_proses';

        $post = $this->input->post();



        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');
     
        if($this->form_validation->run() != false){
            $datainput = array(
                            'nama_merk'=>$post['nama'],
                            'deskripsi'=> $post['deskripsi'],
                            'slug'=> slug($post['nama'])
                            );
            $this->web_model->insert('merk',$datainput);
            $this->session->set_flashdata('sukses','Data berhasil ditambahkan.');

            redirect ('admin/merk');  
        } else {
            $data['p'] = 'admin/v_form_merk';
            $this->load->view('templateadmin',$data);
        }

    }


    public function edit_merk($id=false) {
        if ($id==false) redirect('nonapl');
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Edit Merk';
        $data['jenis'] = 'Edit';
        $data['aksi'] = 'admin/edit_merk_proses';

        $data['detail'] = $this->web_model->view_row('merk',array('id'=>$id));
        $data['p'] = 'admin/v_form_merk';
        $this->load->view('templateadmin',$data);
    }


    public function edit_merk_proses() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}

        $post = $this->input->post();


        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');
     
        if($this->form_validation->run() != false){
            $datainput = array(
                            'nama_merk'=>$post['nama'],
                            'deskripsi'=> $post['deskripsi'],
                            'slug'=> slug($post['nama'])
                            );
            $this->web_model->update('merk',$datainput,array('id'=>$post['id']));
            $this->session->set_flashdata('sukses','Data berhasil diupdate.');

            redirect ('admin/merk');  
        } else {
            $data['p'] = 'admin/v_form_merk';
            $this->load->view('templateadmin',$data);
        }

        //$this->load->view('templateadmin',$data);
    }

    public function hapus_merk($id) {
       $ygdihapus = array('id'=>$id);
       
       $hps = $this->web_model->delete('merk',$ygdihapus);
       $this->session->set_flashdata('sukses','Data berhasil dihapus.');
       redirect('admin/merk');
    }





    /* 
    ==============================
    Kategori 
    ==============================
    */

    public function kategori() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Data Kategori';
        $data['datanya'] = $this->web_model->ambil_kategori('desc');
        $data['p'] = 'admin/v_list_kategori';
        $this->load->view('templateadmin',$data);
    }

    public function tambah_kategori() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['jenis'] = 'Tambah';
        $data['judul'] = 'Tambah Kategori';
        $data['aksi'] = 'admin/tambah_kategori_proses';
        $data['p'] = 'admin/v_form_kategori';
        $this->load->view('templateadmin',$data);
    }

    public function tambah_kategori_proses() { // Tambah Kategori Proses
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}

        $post = $this->input->post();

        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');
     
        if($this->form_validation->run() != false){
            $datainput = array(
                            'nama_kategori'=>$post['nama'],
                            'deskripsi'=> $post['deskripsi'],
                            'slug'=> slug($post['nama'])
                            );
            $this->web_model->insert('kategori',$datainput);
            $this->session->set_flashdata('sukses','Data berhasil ditambahkan.');

            redirect ('admin/kategori');  
        } else {
            $data['p'] = 'admin/v_form_kategori';
            $this->load->view('templateadmin',$data);
        }
    }


    public function edit_kategori($id=false) { //Edit Kategori
        if ($id==false) redirect('nonapl');
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Edit Kategori';
        $data['jenis'] = 'Edit';
        $data['aksi'] = 'admin/edit_kategori_proses';

        $data['detail'] = $this->web_model->view_row('kategori',array('id'=>$id));
        $data['p'] = 'admin/v_form_kategori';
        $this->load->view('templateadmin',$data);
    }


    public function edit_kategori_proses() { //Edit Kategori Proses
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {redirect('nonapl');} 

        if ($this->session->userdata('role')!=1) {redirect('admin/login');}

        $post = $this->input->post();


        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');
     
        if($this->form_validation->run() != false){
            $datainput = array(
                            'nama_kategori'=>$post['nama'],
                            'deskripsi'=> $post['deskripsi'],
                            'slug'=> slug($post['nama'])
                            );
            $this->web_model->update('kategori',$datainput,array('id'=>$post['id']));
            $this->session->set_flashdata('sukses','Data berhasil diupdate.');

            redirect ('admin/kategori');  
        } else {
            $data['p'] = 'admin/v_form_kategori';
            $this->load->view('templateadmin',$data);
        }

        //$this->load->view('templateadmin',$data);
    }

    public function hapus_kategori($id) {
       $ygdihapus = array('id'=>$id);
       
       $hps = $this->web_model->delete('kategori',$ygdihapus);

       $this->session->set_flashdata('sukses','Data berhasil dihapus.');

       redirect('admin/kategori');
    }





    /* 
    ==============================
    Produk 
    ==============================
    */

    public function produk() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Data Produk';

        $j = $this->input->get('j');
        $keyword = $this->input->get('key');

        if ($j == 'k') {
            $jenis_data = 'kategori';
            $data['detail'] = $this->web_model->view('kategori',array('slug'=>$keyword));
        } elseif ($j == 'm') {
            $jenis_data = 'merk';
            $data['detail'] = $this->web_model->view('merk',array('slug'=>$keyword));
        } else {
            $jenis_data = false;
            $keyword = false;
        }

        $jumlah_produk = $this->web_model->get_jumlah_produk($jenis_data,$keyword);

            // echo $this->db->last_query(); die();


        if ($jumlah_produk == 0) {
            $data['text_judul'] = 'Produk tidak ditemukan';
        } elseif ($j == 'k') {
            $data['text_judul'] = 'Ditemukan '.$jumlah_produk.' Produk Dengan Kategori '.$data['detail'][0]->nama_kategori;
        } elseif ($j == 'm') {
           $data['text_judul'] = 'Ditemukan '.$jumlah_produk.' Produk Dengan Merk '.$data['detail'][0]->nama_merk;
        } else {
            $data['text_judul'] = 'Ditemukan '.$jumlah_produk.' Produk';
        }

            // echo $jumlah_produk; die();

        $config['base_url'] = base_url().'admin/produk/';
        $config['total_rows'] =  $jumlah_produk;
        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        // $config['page_query_string'] = TRUE;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="page-item disabled"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

            // $config['anchor_class'] = 'class="page-link"';
            //$config['anchor_class'] = 'class="page-link" ';

        $config['attributes'] = array('class' => 'page-link');
        $config['reuse_query_string'] = true;

        $this->pagination->initialize($config);

        $p = $this->uri->segment(3,0);
        $offset = ($p>1) ? ($p * $config['per_page']) - $config['per_page'] : 0;

        $data['pagination'] = $this->pagination->create_links();

        $data['datanya'] = $this->web_model->ambil_produk($jenis_data,$config['per_page'],$offset,$keyword);
        //$data['merk'] =  $this->web_model->ambil_merk();

        $data['jumlah_produk'] = $jumlah_produk;

        //$data['datanya'] = $this->web_model->ambil_produk('desc');
        $data['p'] = 'admin/v_list_produk';
        $this->load->view('templateadmin',$data);
    }

    public function tambah_produk() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['jenis'] = 'Tambah';
        $data['judul'] = 'Tambah Produk';
        $data['aksi'] = 'admin/tambah_produk_proses';

        $data['kategori'] = $this->web_model->get('kategori');
        $data['merk'] = $this->web_model->get('merk');

        $data['p'] = 'admin/v_form_produk';
        $this->load->view('templateadmin',$data);
    }

    public function tambah_produk_proses() { // Tambah Produk Proses
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}

        $post = $this->input->post();

        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');


        $harga = (int)str_replace('.', '', $post['harga']);
        $stok = $post['stok'];
        $slug = slug($post['nama']);

        if($this->form_validation->run() != false) { //jika validasi benar

            //upload gambar
            $config['upload_path']   = './assets/images/produk/'; 
            $config['allowed_types'] = 'gif|jpg|jpeg|png'; 
            $config['max_size']      = 5000;
            $config['file_name'] = $slug;


            $this->load->library('upload', $config);


            if ( ! $this->upload->do_upload('gambar')) {
                $error = array('error' => $this->upload->display_errors()); 
                print_r($error); die();
                // $this->load->view('imageUploadForm', $error); 
            } else { 

                $dataupload = $this->upload->data();
                $this->resizeImage($dataupload['file_name']);


                //membuat data input
                $datainput = array(
                                'id_kategori'=>$post['id_kategori'],
                                'id_merk'=>$post['id_merk'],
                                'nama_produk'=>$post['nama'],
                                'deskripsi'=> $post['deskripsi'],
                                'slug'=> $slug,
                                'harga'=>$harga,
                                'stok'=>$stok,
                                'gambar_utama'=> $dataupload['file_name']
                                );

                //proses insert ke table
                $this->web_model->insert('produk',$datainput);
                $this->session->set_flashdata('sukses','Data berhasil ditambahkan.');

                redirect ('admin/produk');
            } 
              
        } else { //jika validasi salah
            $data['p'] = 'admin/v_form_produk';
            $this->load->view('templateadmin',$data);
        }
    }


    public function resizeImage($filename) {
        $source_path = './assets/images/produk/' . $filename;
        $target_path = './assets/images/produk/thumbs/';

        $config_manip = array(
            // Large Image
            array(
                'image_library' => 'gd2',
                'source_image'  => $source_path,
                'new_image'     => $source_path,
                'maintain_ratio'=> FALSE,
                'width'         => 480,
                'height'        => 480,
                'overwrite'     => TRUE,
                ),

            // Thumb
            array(
                'image_library' => 'gd2',
                'source_image'  => $source_path,
                'new_image'     => $target_path,
                'maintain_ratio'=> FALSE,
                'thumb_marker'  => '_thumb',
                'width'         => 150,
                'height'        => 150,
                'overwrite'     => TRUE,
                ),
        );
 
        $this->load->library('image_lib', $config_manip[0]);

        foreach ($config_manip as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                //echo $this->image_lib->display_errors();
                return false;
            }

            $this->image_lib->clear();
        }
    
    }


    public function edit_produk($id=false) { //Edit Produk
        if ($id==false) redirect('nonapl');
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Edit Produk';
        $data['jenis'] = 'Edit';
        $data['aksi'] = 'admin/edit_produk_proses';

        $data['kategori'] = $this->web_model->get('kategori');
        $data['merk'] = $this->web_model->get('merk');

        $data['detail'] = $this->web_model->view_row('produk',array('id'=>$id));
        $data['p'] = 'admin/v_form_produk';
        $this->load->view('templateadmin',$data);
    }


    public function edit_produk_proses() { //Edit Produk Proses
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {redirect('nonapl');} 

        if ($this->session->userdata('role')!=1) {redirect('admin/login');}

        $post = $this->input->post();
        $id = $post['id'];

        $harga = (int)str_replace('.', '', $post['harga']);
        $stok = $post['stok'];
        $slug = slug($post['nama']);

        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');
     
        if($this->form_validation->run() != false){

            $detail = $this->web_model->view_row('produk',array('id'=>$id));

            if (!empty($_FILES['gambar']['name'])) { //jika gambar baru ada dari form
                //lakukan upload
                $config['upload_path']   = './assets/images/produk/'; 
                $config['allowed_types'] = 'gif|jpg|jpeg|png'; 
                $config['max_size']      = 5000;
                $config['file_name'] = $slug;
                $config['overwrite'] = TRUE;


                $this->load->library('upload', $config);


                if ( ! $this->upload->do_upload('gambar')) {
                    $error = array('error' => $this->upload->display_errors()); 
                    print_r($error); die();
                    // $this->load->view('imageUploadForm', $error); 
                } else { 

                    $dataupload = $this->upload->data();
                    $this->resizeImage($dataupload['file_name']);
                }

                $gambar_baru = $dataupload['file_name'];

            } else { //jika tidak memilih gambar baru
                $gambar_baru = $detail->gambar_utama;
            }
            
            //membuat data input
            $datainput = array(
                'id_kategori'=>$post['id_kategori'],
                'id_merk'=>$post['id_merk'],
                'nama_produk'=>$post['nama'],
                'deskripsi'=> $post['deskripsi'],
                'slug'=> $slug,
                'harga'=>$harga,
                'stok'=>$stok,
                'gambar_utama'=> $gambar_baru
            );


            $this->web_model->update('produk',$datainput,array('id'=>$id));
            $this->session->set_flashdata('sukses','Data berhasil diupdate.');

            redirect ('admin/produk');  
        } else {
            $data['p'] = 'admin/v_form_produk';
            $this->load->view('templateadmin',$data);
        }

        //$this->load->view('templateadmin',$data);
    }

    public function hapus_produk($id) {
       $ygdihapus = array('id'=>$id);

       $detail = $this->web_model->view_row('produk',$ygdihapus);
       
       $image_ori = 'assets/images/produk/'.$detail->gambar_utama;
       $image_thumb = 'assets/images/produk/thumbs/'.$detail->gambar_utama;


       unlink($image_ori);
       unlink($image_thumb);
       
       $hps = $this->web_model->delete('produk',$ygdihapus);

       $this->session->set_flashdata('sukses','Data berhasil dihapus.');

       redirect('admin/produk');
    }







    public function transaksi() {
        $data['judul'] = 'Data Transaksi';
        $data['datanya'] = $this->web_model->ambil_transaksi_admin('desc');

        // print_r_pre($data['datanya']);
        // echo $this->db->last_query(); 
        // die();

        $data['p'] = 'admin/v_list_transaksi';
        $this->load->view('templateadmin',$data);   
    }

    public function edit_transaksi($id=false) { //Edit Transaksi
        if ($id==false) redirect('nonapl');
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $data['judul'] = 'Edit Transaksi';
        $data['jenis'] = 'Edit';
        $data['aksi'] = 'admin/edit_transaksi_proses';

        $data['status_transaksi'] = $this->web_model->get('status_transaksi');

        $data['detail'] = $this->web_model->ambil_transaksi_admin(false,$id);
        // print_r_pre($data['detail']);
        $data['p'] = 'admin/v_form_transaksi';
        $this->load->view('templateadmin',$data);
    }

    public function edit_transaksi_proses() {
        if ($this->session->userdata('role')!=1) {redirect('admin/login');}
        $post = $this->input->post();

        $id = $post['id'];

        $datainput = array(
                        'status'=>$post['status']
                        );
        $this->web_model->update('transaksi',$datainput,array('id'=>$id));
        $this->session->set_flashdata('sukses','Status berhasil diupdate.');

        redirect ('admin/edit_transaksi/'.$id);  

    }

}
