<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//require_once(APPPATH . 'modules/auth/controllers/Base_global.php');

class Member extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('web_model'));
        $this->load->helper('captcha');
        $this->username = $this->session->userdata('username');

        $this->modul = $this->uri->segment(1);
     
    }


    function index () {
        $data['pengguna'] = $this->web_model->view('pengguna',array('username'=>$this->username));
        $data['transaksi'] = $this->web_model->ambil_transaksi('member',$this->username);

        // print_r_pre($data['pengguna']); 
        $data['p'] = 'member/v_member';
        $this->load->view('template',$data);
    }

    function buat_captcha() {
        $vals = array(
        'img_path'      => './captcha/',
        'img_url'       => base_url('captcha'),
        // 'font_path'     => './path/to/fonts/texb.ttf',
        'img_width'     => '150',
        'img_height'    => 40,
        'expiration'    => 7200,
        'word_length'   => 3,
        // 'font_size'     => 12,
        'img_id'        => 'Imageid',
        'pool'          => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        );
        $cap = create_captcha($vals);

        return $cap;
    }

    function daftar () {

        $data['cap'] = $this->buat_captcha();

        // print_r($data['cap']); die();

        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$data['cap']['word']);

        $data['p'] = 'member/v_daftar';
        $this->load->view('template',$data);
    }

    function daftar_proses () {
        $post = $this->input->post();
        // print_r_pre($post); die();
        if (!empty($post)) {
            $inputCaptcha = $this->input->post('captcha');
            $sessCaptcha = $this->session->userdata('captchaCode');

            // print_r_pre($this->session->all_userdata()); die();

            if ($post['pass1'] != $post['pass2']) {
                echo '<div align="center">';
                echo 'Konfirmasi Password Salah..<br>';
                echo '<a href="javascript:history.back()">Ulangi</a>';
                echo '</div>';
                die();  
            }

            //validasi
            $this->form_validation->set_rules('username', 'Username', 'required|max_length[50]|min_length[5]');
            $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]');
            $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|max_length[255]');
            $this->form_validation->set_rules('no_hp', 'Nomor HP', 'required|max_length[20]');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[30]');
            $this->form_validation->set_rules('pass1', 'Password', 'required|min_length[6]|max_length[255]');
            $this->form_validation->set_rules('pass2', 'Konfirmasi Password', 'required|min_length[6]|max_length[255]');

            if ($this->form_validation->run() == FALSE) {
                echo '<div align="center">';
                echo validation_errors().'<br>';
                echo '<a href="javascript:history.back()">Ulangi</a>';
                echo '</div>';
                die();
            } else {

                $datausername = $this->web_model->view('pengguna',array('username'=>$post['username']));

                //print_r_pre($datausername); die();

                if ($datausername[0]->username != '') {
                    echo '<div align="center">';
                    echo 'Username tidak tersedia!<br>';
                    echo '<a href="javascript:history.back()">Ulangi</a>';
                    echo '</div>';
                    die(); 
                }

                //$id_max = $this->web_model->ambil_id_max('transaksi');
                //$id_baru = $id_max + 1;
                //$nomor_transaksi = date('Ymd').'-'.$id_baru;


                $password = md5($post['pass1']);

                $datainput = array(
                            'username'=> $post['username'],
                            'password'=> $password,
                            'nama_lengkap'=> $post['nama_lengkap'],
                            'jenis_kelamin'=> $post['jk'],
                            'no_hp'=> $post['no_hp'],
                            'alamat_lengkap'=>$post['alamat'],
                            'email'=> $post['email'],
                            'role' => 2,
                            'is_divalidasi'=> 1    
                            );

                $this->web_model->insert('pengguna',$datainput);

                $this->session->set_flashdata('sukses','Pendaftaran Berhasil. Silakan Login');

                redirect ('member/login');   
            }

            
        } else {
            redirect('member/daftar');
        }


    }

    public function login() {

        $r = $this->input->get('r');

        $post = $this->input->post();
        // print_r_pre($post);
        if (!empty($post)) {
            $ceklogin = $this->web_model->cekLogin($post['username'],$post['pass']);
            //echo $this->db->last_query(); die();

            if ($ceklogin == null) {

                $back = "username";
                $msg = "Username atau password yang Anda input salah.";
                // $this->Login_model->update_failed_login($this->input->post('username'));
                // echo json_encode(array('back' => $back, 'msg' => $msg));

                redirect('member/login');
            } else {
                $datasession = array(
                    "logged_in" => TRUE,
                    "username" => $ceklogin[0]->username,
                    "nama_lengkap" => $ceklogin[0]->nama_lengkap,
                    "role" => $ceklogin[0]->role,
                    "is_aktif" => $ceklogin[0]->is_aktif
                );
                $this->session->set_userdata($datasession);

                if ($r == 'checkout') {
                    redirect('produk/checkout');
                } else {
                    $this->session->set_flashdata('sukses','Login Berhasil.');
                    redirect('member');
                }

            }
            
        } else {
            $data['p'] = 'web/v_login';
            $this->load->view('template',$data);
        }
       
    }

    public function logout () {
        session_destroy();

        $this->session->set_flashdata('sukses','Anda Berhasil Logout.');

        redirect('home');
    }

    public function transaksi() {
        $data['transaksi'] = $this->web_model->ambil_transaksi('member',$this->username);
        
        $data['p'] = 'member/v_transaksi';
        $this->load->view('template',$data);   
    }

}
