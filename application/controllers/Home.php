<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent:: __construct();
        $this->load->model(array("web_model"));



    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		$data['kategori'] = $this->web_model->get('kategori');
		$data['merk'] = $this->web_model->get('merk');

		$data['produk_populer'] = $this->web_model->ambil_produk('populer',4,0);
		$data['produk_terbaru'] = $this->web_model->ambil_produk('terbaru',4,0);

		// print_r_pre($data['produk_populer']); die();

		$data['p'] = 'web/v_home';
		$this->load->view('template',$data);
	}
}
