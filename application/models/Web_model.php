<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function insert($tablename, $data1) {
        return $this->db->insert($tablename, $data1);
    }

    function update($tablename, $kolom, $where) {
        $this->db->where($where);
        return $this->db->update($tablename, $kolom);
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function view_row($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->row();
    }

    function delete($tablename, $where) {
        $this->db->where($where);
        return $this->db->delete($tablename);
    }

    function ambil_produk($jenis = false, $limit = 0, $offset = 0, $keyword= false) {

        if ($jenis=='populer') {
            $order = "ORDER BY dilihat desc";
        } elseif ($jenis=='terbaru') {
           $order = " ORDER by id DESC";
        } else {
            $order = " ORDER by id DESC";
        }


        if ($jenis == 'cari') {
            $where = " WHERE p.product_name LIKE '%$q%' OR p.description LIKE '%$q%' ";
        } elseif ($jenis == 'kategori') {
            $where = " WHERE k.slug = '$keyword' ";
        } elseif ($jenis == 'merk') {
            $where = " WHERE m.slug = '$keyword' ";
        } else {
            $where = " ";
        }

        $sql = "SELECT
                    p.*,
                    SUBSTRING_INDEX(g.gambar, ',', 1) AS gambar1,
                    k.nama_kategori AS nama_kategori
                FROM
                    produk p
               LEFT JOIN (
                            SELECT
                                gambar.id_produk,
                                GROUP_CONCAT(
                                    gambar.gambar ORDER BY gambar.id SEPARATOR ','
                                ) AS gambar
                            FROM
                                gambar
                            GROUP BY
                                id_produk
                        ) g ON g.id_produk = p.id
                LEFT JOIN kategori k ON k.id = p.id_kategori
                LEFT JOIN merk m ON m.id = p.id_merk";

        $sql.= $where;

        $sql.= $order;

        $sql.= " limit $offset,$limit ";

        return $this->db->query($sql)->result();
    }

    function detail_produk($slug = false) {
        $order = " ";
        $where = " WHERE p.slug = '$slug' ";
       

        $sql = "SELECT
                    p.*,
                    SUBSTRING_INDEX(g.gambar, ',', 1) AS gambar,
                    k.nama_kategori AS nama_kategori
                FROM
                    produk p
               LEFT JOIN (
                            SELECT
                                gambar.id_produk,
                                GROUP_CONCAT(
                                    gambar.gambar ORDER BY gambar.id SEPARATOR ','
                                ) AS gambar
                            FROM
                                gambar
                            GROUP BY
                                id_produk
                        ) g ON g.id_produk = p.id
                LEFT JOIN kategori k ON k.id = p.id_kategori";

        $sql.= $where;

        $sql.= $order;

        return $this->db->query($sql)->row();
    }


    function ambil_produk_sejenis($id_produk = false, $id_kategori = false, $id_merk = false) {

        $where = " WHERE p.id <> $id_produk AND k.id = $id_kategori AND m.id = $id_merk ";


        $sql = "SELECT
                    p.*,
                    SUBSTRING_INDEX(g.gambar, ',', 1) AS gambar1,
                    k.nama_kategori AS nama_kategori
                FROM
                    produk p
               LEFT JOIN (
                            SELECT
                                gambar.id_produk,
                                GROUP_CONCAT(
                                    gambar.gambar ORDER BY gambar.id SEPARATOR ','
                                ) AS gambar
                            FROM
                                gambar
                            GROUP BY
                                id_produk
                        ) g ON g.id_produk = p.id
                LEFT JOIN kategori k ON k.id = p.id_kategori
                LEFT JOIN merk m ON m.id = p.id_merk";

        $sql.= $where;

        return $this->db->query($sql)->result();
    }


    function get_jumlah_produk($jenis = false, $slug=false) {

        $this->db->from('produk p');

        if ($jenis == 'cari') {
            $this->db->like('nama_produk', $slug);
            $this->db->or_like('deskripsi', $slug);
        } elseif ($jenis == 'kategori') {
            $this->db->join('kategori k','k.id = p.id_kategori');
            $this->db->where('k.slug',$slug);
        } elseif ($jenis == 'merk') {
            $this->db->join('merk m','m.id = p.id_merk');
            $this->db->where('m.slug',$slug);
        }

        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }



    //Member
    function cekLogin($username, $password,$role = false) {
        if ($role == 'admin') {
            $this->db->where('role',1);
        }

        $this->db->select('*');
        $this->db->from('pengguna');
        // $this->db->join('dosen', 'dosen.nidn = user.username','left');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }
        
    }

    function ambil_transaksi($jenis=false, $username = false) {
        if ($jenis == 'member') {
            $where = " WHERE transaksi.username = '$username' ";
        }

        $order = " ORDER BY transaksi.id DESC";

        $sql = "
            SELECT transaksi.*, td.id_produk, td.nama_produk FROM transaksi 
            LEFT JOIN (
                SELECT 
                    td.id_transaksi,
                    GROUP_CONCAT(DISTINCT id_produk
                        ORDER BY id_produk ASC
                        SEPARATOR ';') as id_produk,
                        GROUP_CONCAT(DISTINCT p.nama_produk
                        ORDER BY p.id ASC
                        SEPARATOR ';') as nama_produk
                FROM
                    transaksi_detail td
                    LEFT JOIN produk p ON P.id  = td.id_produk
                    GROUP BY id_transaksi
                ) AS td ON td.id_transaksi = transaksi.id
        ";

        $sql.= $where;
        $sql.= $order;

        return $this->db->query($sql)->result();
        
    }


    //Ambil id max
    function ambil_id_max($nama_tabel=false) {
        $this->db->select_max('id');
        $hasil = $this->db->get($nama_tabel);
        if ($hasil->row('id') == NULL) {
            return 0;
        } else {  
            return $hasil->row('id');    
        }
    }


    //ambil merk
    function ambil_merk($order=false) {

        $qryorder = "";

        if (strtolower($order) == 'desc') {
            $qryorder = " ORDER BY id DESC"; 
        }
        

        $sql = "SELECT
                    m.id,
                    m.slug,
                    m.nama_merk,
                    m.deskripsi,
                    count(p.id) as jumlah_produk
                FROM
                    produk p
                RIGHT JOIN merk m ON m.id = p.id_merk
                GROUP BY 
                    m.id,
                    m.slug,
                    m.deskripsi,
                    m.nama_merk
                ";
        $sql.=$qryorder;


        return $this->db->query($sql)->result();
    }


    //ambil Kategori
    function ambil_kategori($order=false) {
        $qryorder = "";

        if (strtolower($order) == 'desc') {
            $qryorder = " ORDER BY id DESC"; 
        }
        

        $sql = "SELECT
                    k.id,
                    k.slug,
                    k.nama_kategori,
                    k.deskripsi,
                    count(p.id) as jumlah_produk
                FROM
                    produk p
                RIGHT JOIN kategori k ON k.id = p.id_kategori
                GROUP BY 
                    k.id,
                    k.slug,
                    k.deskripsi,
                    k.nama_kategori
                ";
        $sql.=$qryorder;

        return $this->db->query($sql)->result();
    }

    //ambil Transaksi Admin
    function ambil_transaksi_admin($order=false, $id=false) {
        $qryorder = "";
        $qrywhere = "";

        if (strtolower($order) == 'desc') {
            $qryorder = " ORDER BY id DESC"; 
        }

        if ($id != false) {
            $qrywhere = " WHERE t.id = $id "; 
        }
        

        $sql = "SELECT
                    t.id,
                    t.nomor_transaksi,
                    t.tanggal_transaksi,
                    t.username,
                    t.nama_penerima,
                    t.alamat_pengiriman,
                    td.produk_dibeli,
                    t.status,
                    t.total
                FROM
                    transaksi t
                LEFT JOIN ( 
                            SELECT
                            td.id_transaksi,
                            GROUP_CONCAT(
                                    p.nama_produk ORDER BY td.id SEPARATOR ','
                                ) AS produk_dibeli
                            from transaksi_detail td 
                            LEFT JOIN produk p ON p.id = td.id_produk
                            GROUP BY td.id_transaksi
                        ) td ON t.id = td.id_transaksi

                ";
        $sql.=$qrywhere;
        $sql.=$qryorder;

        if ($id != false) {
            return $this->db->query($sql)->row(); 
        } else {
            return $this->db->query($sql)->result();
        }

        
    }
    

    function get_image_by_product_id($product_id = 0) {
        $sql = "SELECT * from image WHERE product_id = $product_id";
        return $this->db->query($sql)->row();
    }

    function get_category() {
        $sql = "SELECT * from category ORDER BY id";
        return $this->db->query($sql)->result();
    }
    
}
?>

