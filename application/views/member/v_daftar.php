<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">

<!-- ============================ COMPONENT REGISTER   ================================= -->
	<div class="card mx-auto" style="max-width:520px; margin-top:20px;">
      <article class="card-body">
		<header class="mb-4"><h4 class="card-title">Pendaftaran</h4></header>
		<?php echo form_open('member/daftar_proses');?>
				<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" class="form-control col-md-5" placeholder="" required=""minlength="5">
				</div> <!-- form-group end.// -->

				<div class="form-group">
					<label>Nama Lengkap</label>
					<input type="text" name="nama_lengkap" class="form-control col-md-7" placeholder="" required="">
				</div> <!-- form-group end.// -->

				<div class="form-group">
					<label>Nomor HP</label>
					<input type="text" name="no_hp" class="form-control col-md-7" placeholder="" maxlength="20" required="">
				</div> <!-- form-group end.// -->


				<div class="form-group">
					<label>Email</label>
					<input type="email" name = "email" class="form-control" placeholder="" required="" minlength="7">
					<small class="form-text text-muted">Kami tidak akan membagikan email Anda kepada orang lain.</small>
				</div> <!-- form-group end.// -->

				<div class="form-group">
					<label>Alamat</label>
					<textarea name="alamat" class="form-control" rows="3" required="" minlength="30"><?php //echo $data_member[0]->alamat_lengkap;?></textarea>
				</div>

				<?php //echo $cap['image'];?>
		
				<!-- <div style="margin-top: -10px;"></div> -->
				<!-- <div class="form-group">
					<label>Masukan Kode Diatas</label>
					<input type="text" name="captcha" class="form-control col-sm-3" placeholder="" required="">
				</div> -->

				<div class="form-group">
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="jk" required = "" value="l">
					  <span class="custom-control-label"> Laki-laki </span>
					</label>
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="jk" required="" value="p">
					  <span class="custom-control-label"> Perempuan </span>
					</label>
				</div> <!-- form-group end.// -->
				
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Buat Password</label>
					    <input class="form-control" type="password" name="pass1" minlength="6" required="">
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>Ulangi Password</label>
					    <input class="form-control" type="password" name="pass2" minlength="6" required="">
					</div> <!-- form-group end.// -->  
				</div>
			    <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Daftar  </button>
			    </div> <!-- form-group// -->      
			    <div class="form-group"> 
		            <label class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" checked=""> <div class="custom-control-label"> Saya setuju dengan <a href="#">Syarat dan Kebijakan</a>  </div> </label>
		        </div> <!-- form-group end.// -->           
			</form>
		</article><!-- card-body.// -->
    </div> <!-- card .// -->
    <p class="text-center mt-4">Sudah punya akun? <a href="<?php echo base_url('member/login')?>">Log In</a></p>
    <br><br>
<!-- ============================ COMPONENT REGISTER  END.// ================================= -->


</section>
<!-- ========================= SECTION CONTENT END// ========================= -->