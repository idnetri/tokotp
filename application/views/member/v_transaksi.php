<section class="section-content padding-y bg">
<div class="container">
	<?php tampilkan_notifikasi(); ?>
<div class="row">
	<aside class="col-md-3">
		<!--   SIDEBAR   -->
		<ul class="list-group">
			<a class="list-group-item active" href="#"> Ringkasan Akun </a>
			<a class="list-group-item" href="<?php echo base_url('member/transaksi');?>"> Pesanan Saya </a>
		</ul>
		<br>
		<a class="btn btn-light btn-block" href="" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-power-off"></i> <span class="text">Log out</span> </a> 
		<!--   SIDEBAR .//END   -->
	</aside>
	<main class="col-md-9">
		<article class="card">
		<header class="card-header">
			<strong class="d-inline-block mr-3">Data Transaksi</strong>
		</header>

		<div class="table-responsive">
		<table class="table table-hover">
			<?php foreach ($transaksi as $row) { ?>
				<tr>
				<td> 
					<p class="title mb-0"><?php echo $row->nama_produk;?> </p>
					<var class="price text-muted"><?php echo format_idr($row->total);?></var>
				</td>
				<!-- <td> Seller <br> Nike clothing </td> -->
				<td width="100"> 
					<!-- <a href="#" class="btn btn-outline-primary">Track order</a> -->
					<a href="#" class="btn btn-light"> Detail </a> 
				</td>
			</tr>
				
			<?php } ?>
			
		</table>
		</div> <!-- table-responsive .end// -->
		</article> <!-- order-group.// --> 
	</main>
</div> <!-- row.// -->
<!-- =========================  COMPONENT MYORDER 1 END.// ========================= --> 
</div>
</section>