<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy M.">

<title><?php echo (isset($judul))?$judul:'Halaman Admin';?></title>

<!-- jQuery -->
<script src="<?php echo base_url('assets/');?>js/jquery-2.0.0.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="<?php echo base_url('assets/');?>js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="<?php echo base_url('assets/');?>css/bootstrap.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="<?php echo base_url('assets/');?>fonts/fontawesome/css/all.min.css" type="text/css" rel="stylesheet">

<!-- custom style -->
<link href="<?php echo base_url('assets/');?>css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/');?>css/responsive.css" rel="stylesheet" />

<!-- Numeral JS -->
<script src="<?php echo base_url('assets/');?>js/numeral.js" type="text/javascript"></script>

<!-- custom javascript -->
<script src="<?php echo base_url('assets/');?>js/script.js" type="text/javascript"></script>

</head>
<body>

<header class="section-header">
<section class="header-main border-bottom">
<div class="container">
	<a href="<?php echo base_url('admin');?>" class="brand-wrap"><img class="logo" src="<?php echo base_url('assets/');?>images/logo.png"></a>
</div> <!-- container.// -->
</section>
</header> <!-- section-header.// -->


<section class="section-content padding-y bg">
<div class="container">
	<?php tampilkan_notifikasi();?>
<!-- =========================  COMPONENT MYORDER 1 ========================= --> 
<div class="row">
	<aside class="col-md-3">
		<!--   SIDEBAR   -->
		<ul class="list-group">
			<a class="list-group-item <?php echo (uri_string()=='admin')?'active':'';?>" href="<?php echo base_url('admin');?>"> Home </a>
			<a class="list-group-item <?php echo (uri_string()=='admin/merk')?'active':'';?>" href="<?php echo base_url('admin/merk');?>"> Merk </a>
			<a class="list-group-item <?php echo (uri_string()=='admin/kategori')?'active':'';?>" href="<?php echo base_url('admin/kategori');?>"> Kategori </a>
			<a class="list-group-item <?php echo (uri_string()=='admin/produk')?'active':'';?>" href="<?php echo base_url('admin/produk');?>"> Produk </a>
			<a class="list-group-item <?php echo (uri_string()=='admin/transaksi')?'active':'';?>" href="<?php echo base_url('admin/transaksi');?>"> Transaksi </a>
			<!-- <a class="list-group-item <?php echo (uri_string()=='admin/pengaturan')?'active':'';?>" href="<?php //echo base_url('admin/pengaturan');?>"> Pengaturan </a> -->
		</ul>
		<br>
		<a class="btn btn-light btn-block" href="<?php echo base_url('admin/logout');?>"> <i class="fa fa-power-off"></i> <span class="text">Log out</span> </a> 
		<!--   SIDEBAR .//END   -->
	</aside>


	<?php $this->load->view($p); ?>


</div> <!-- row.// -->
<!-- =========================  COMPONENT MYORDER 1 END.// ========================= --> 



</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->

<!-- ========================= FOOTER ========================= -->
<footer class="section-footer border-top padding-y">
	<div class="container">
		<p class="float-md-right"> 
			© Copyright 2020 All rights reserved
		</p>
		<p>
			Developed By : <a href="#" target="_blank">Tri</a>
		</p>
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


<script type="text/javascript">
	//$(".numeric").numeric({ decimal : ".",  negative : false, scale: 3 }); 
	$('.numericx').inputmask({
	  alias: 'numeric', 
	  allowMinus: false,  
	  digits: 2, 
	  max: 999.99
	});  
</script>
</body>
</html>