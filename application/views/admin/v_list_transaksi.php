<main class="col-md-9">

	<div class="card">
		<article class="card-body">
		<header class="mb-8">
			<h4 class="card-title">Data Transaksi</h4>
		</header>


		<table class="table">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">Nama</th>
		      <th scope="col">Tanggal Transaksi</th>
		      <th scope="col">Produk Dibeli</th>
		      <th scope="col">Total Harga</th>
		      <th scope="col">Status</th>
		      <th scope="col" width="15%">Aksi</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php 
		  	if (empty($datanya)) {
		  		echo '<td colspan="4" align="center">Data Masih Kosong!</td>';
		  	} else {
		  		foreach ($datanya as $key => $row) { ?>
		  		
		  		<tr>
			      <td scope="row"><?php echo ($key+1);?></td>
			      <td><?php echo $row->nama_penerima;?></td>
			      <td><?php echo $row->tanggal_transaksi;?></td>
			      <td><?php echo $row->produk_dibeli;?></td>
			      <td class="float-right"><?php echo format_angka($row->total);?></td>
			      <td>
			      	<?php 
			      	$status = $row->status;

			      	switch ($status) {
			      		case 'baru':
			      			$style = 'alert-light';
			      			break;

			      		case 'menunggu pembayaran':
			      			$style = 'alert-warning';
			      			break;

			      		case 'dikirim':
			      			$style = 'alert-info';
			      			break;

			      		case 'selesai':
			      			$style = 'alert-success';
			      			break;

			      		case 'dibatalkan':
			      			$style = 'alert-danger';
			      			break;
			      		
			      		default:
			      			$style = '';
			      			break;
			      	}
			      	 ?>
			      	<div class="alert <?php echo $style;?>">
			      		<center><?php echo $row->status;?></center>
			      	</div>
			      </td>
			      <td><a href="<?php echo base_url('admin/edit_transaksi/'.$row->id);?>" class="btn btn-primary btn-sm">Ubah Status</a>
			      </td>
			    </tr>

		  	<?php

		  		}
		  	}

		  	?>
		  </tbody>
		</table>


	</div> <!-- card.// -->

</main>




