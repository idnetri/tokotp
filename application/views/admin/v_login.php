<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy M.">

<title>Halaman Admin</title>

<!-- jQuery -->
<script src="<?php echo base_url('assets/');?>js/jquery-2.0.0.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="<?php echo base_url('assets/');?>js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="<?php echo base_url('assets/');?>css/bootstrap.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="<?php echo base_url('assets/');?>fonts/fontawesome/css/all.min.css" type="text/css" rel="stylesheet">

<!-- custom style -->
<link href="<?php echo base_url('assets/');?>css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/');?>css/responsive.css" rel="stylesheet" />

<!-- custom javascript -->
<script src="<?php echo base_url('assets/');?>js/script.js" type="text/javascript"></script>

</head>
<body>

<header class="section-header">
<section class="header-main border-bottom">
<div class="container">
  <a href="http://bootstrap-ecommerce.com" class="brand-wrap"><img class="logo" src="<?php echo base_url('assets/');?>images/logo.png"></a>
</div> <!-- container.// -->
</section>
</header> <!-- section-header.// -->


<section class="section-content padding-y bg">
<div class="container">

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-conten padding-y" style="min-height:84vh">

<!-- ============================ COMPONENT LOGIN   ================================= -->
  <div class="card mx-auto" style="max-width: 380px; margin-top:10px;">
      <div class="card-body">
      <h4 class="card-title mb-4">Login Admin</h4>
      <?php echo form_open('admin/login');?>
          <!-- <a href="#" class="btn btn-facebook btn-block mb-2"> <i class="fab fa-facebook-f"></i> &nbsp  Sign in with Facebook</a>
          <a href="#" class="btn btn-google btn-block mb-4"> <i class="fab fa-google"></i> &nbsp  Sign in with Google</a> -->
          <div class="form-group">
       <input name="username" class="form-control" placeholder="Username" type="text">
          </div> <!-- form-group// -->
          <div class="form-group">
      <input name="pass" class="form-control" placeholder="Password" type="password">
          </div> <!-- form-group// -->
          
          <div class="form-group">
            <a href="#" class="float-right">Lupa Password?</a> 
            <label class="float-left custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" checked=""> <div class="custom-control-label"> Remember </div> </label>
          </div> <!-- form-group form-check .// -->
          <div class="form-group">
              <button type="submit" name="submit" class="btn btn-primary btn-block"> Login  </button>
          </div> <!-- form-group// -->    
      </form>
      </div> <!-- card-body.// -->
    </div> <!-- card .// -->
     <br><br>
<!-- ============================ COMPONENT LOGIN  END.// ================================= -->


</section>
<!-- ========================= SECTION CONTENT END// =========================

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->

<!-- ========================= FOOTER ========================= -->
<footer class="section-footer border-top padding-y">
  <div class="container">
    <a href="http://bootstrap-ecommerce.com">Bootstrap-ecommerce UI kit</a>
  </div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
</html>


