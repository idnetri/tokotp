<main class="col-md-9">

	<div class="card">
		<article class="card-body">
		<header class="mb-8">
			<h4 class="card-title">Data Merk</h4>
		</header>



		<div class="mb-2 float-right">
	  		<a href="<?php echo base_url('admin/tambah_merk'); ?>" class="btn btn-success ml-md-4"><i class="fa fa-plus"></i> Tambah Merk</a>
	  		<br>
	  	</div>

		<table class="table">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">Nama</th>
		      <th scope="col">Deskripsi</th>
		      <th scope="col" width="15%">Aksi</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php 
		  	if (empty($datanya)) {
		  		echo '<td colspan="4" align="center">Data Masih Kosong!</td>';
		  	} else {
		  		foreach ($datanya as $key => $row) { ?>
		  		
		  		<tr>
			      <th scope="row"><?php echo ($key+1);?></th>
			      <td><?php echo $row->nama_merk;?></td>
			      <td><?php echo $row->deskripsi;?></td>
			      <td><a href="<?php echo base_url('admin/edit_merk/'.$row->id);?>" class="btn btn-primary btn-sm">Edit</a> <a href="<?php echo base_url('admin/hapus_merk/'.$row->id);?>" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?');" class="btn btn-danger btn-sm">Hapus</a></td>
			    </tr>

		  	<?php

		  		}
		  	}

		  	?>
		  </tbody>
		</table>


	</div> <!-- card.// -->

</main>




