<main class="col-md-9">
	<?php //print_r_pre($datanya);?>
	<div class="card">
		<article class="card-body">
		<header class="mb-8">
			<h4 class="card-title">Data Produk</h4>
		</header>



		<div class="mb-2 float-right">
	  		<a href="<?php echo base_url('admin/tambah_produk'); ?>" class="btn btn-success ml-md-4"><i class="fa fa-plus"></i> Tambah Produk</a>
	  		<br>
	  	</div>

		<table class="table">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">Nama</th>
		      <th scope="col">Deskripsi</th>
		      <th scope="col" width="15%">Aksi</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php 
		  	if (empty($datanya)) {
		  		echo '<td colspan="4" align="center">Data Masih Kosong!</td>';
		  	} else {
		  		foreach ($datanya as $key => $row) { ?>
		  		
		  		<tr>
			      <th scope="row"><?php echo ($key+1);?></th>
			      <td><?php echo $row->nama_produk;?></td>
			      <td><?php echo $row->deskripsi;?></td>
			      <td><a href="<?php echo base_url('admin/edit_produk/'.$row->id);?>" class="btn btn-primary btn-sm">Edit</a> <a href="<?php echo base_url('admin/hapus_produk/'.$row->id);?>" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?');" class="btn btn-danger btn-sm">Hapus</a></td>
			    </tr>

		  	<?php

		  		}
		  	}

		  	?>
		  </tbody>
		</table>

		<nav class="mt-4" aria-label="Page navigation sample">
			<?php echo $pagination;?>
		</nav>


	</div> <!-- card.// -->

</main>




