<main class="col-md-9">
	<div class="card">
	<article class="card-body">
	<header class="mb-8">
		<h4 class="card-title"><?php echo $jenis;?> Merk</h4>
	</header>
	<form class="block-register" action="<?php echo base_url($aksi);?>" method="post">
	  <input type="hidden" name="id" value="<?php echo (isset($detail->id))?$detail->id:''; ?>">
	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Nama Merk</label>
	    <div class="col">
	      <input type="text" name="nama" class="form-control col-md-6" placeholder="Apple" required="" value="<?php echo (isset($detail->nama_merk))?$detail->nama_merk:''; ?>">
	      <span class="error text-danger"><?php echo form_error('nama'); ?></span>
	    </div>
	    
	  </div>


	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Deskripsi</label>
	    <div class="col">
	      <textarea name="deskripsi" class="form-control" required="" placeholder="Deskripsi Merk Apple"><?php echo (isset($detail->deskripsi))?$detail->deskripsi:''; ?></textarea>
	      <span class="error text-danger"><?php echo form_error('deskripsi'); ?></span>
	    </div>
	    
	  </div>

	  <div class="float-right">
	  	<a href="<?php echo base_url('admin/merk');?>" class="btn btn-danger">Batal</a>
	  	<button type="submit" class="btn btn-primary">Simpan</button>
	  </div>

	</form>
	</article>
</main>




