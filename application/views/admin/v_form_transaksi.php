<main class="col-md-9">
	<div class="card">
	<article class="card-body">
	<header class="mb-8">
		<h4 class="card-title"><?php echo $jenis;?> Transaksi</h4>
	</header>
	<!-- <form class="block-register" action="<?php //echo base_url($aksi);?>" method="post" > -->
	<?php echo form_open_multipart($aksi,array('class'=>'block-register'));?>
	  <input type="hidden" name="id" value="<?php echo (isset($detail->id))?$detail->id:''; ?>">

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Nomor Transaksi</label>
	    <div class="col">
	      <input type="text" name="nomor_transaksi" class="form-control col-md-6" placeholder="" required="" value="<?php echo (isset($detail->nomor_transaksi))?$detail->nomor_transaksi:''; ?>" disabled="">
	      <span class="error text-danger"><?php echo form_error('nama'); ?></span>
	    </div>
	    
	  </div>

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Nama Penerima</label>
	    <div class="col">
	      <input type="text" name="nama" class="form-control col-md-6" placeholder="" required="" value="<?php echo (isset($detail->nama_penerima))?$detail->nama_penerima:''; ?>" disabled="">
	      <span class="error text-danger"><?php echo form_error('nama'); ?></span>
	    </div>
	    
	  </div>


	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Alamat Pengiriman</label>
	    <div class="col">
	      <textarea name="alamat_pengiriman" class="form-control" required="" placeholder="" disabled=""><?php echo (isset($detail->alamat_pengiriman))?$detail->alamat_pengiriman:''; ?></textarea>
	      <span class="error text-danger"><?php echo form_error('alamat_pengiriman'); ?></span>
	    </div>
	    
	  </div>

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Status</label>
	    <div class="col">
	      <select name="status" class="form-control col-md-6" required="">
	      	<?php
        	foreach ($status_transaksi as $row) { ?>
				<option value="<?php echo $row->status;?>" <?php echo (@$row->status==@$detail->status)?'selected':''?> ><?php echo $row->status;?></option>
	        <?php
	        	}
	        ?>
	      </select>
	      <span class="error text-danger"><?php echo form_error('status'); ?></span>
	    </div>
	    
	  </div>

	  

	  <div class="float-right">
	  	<a href="<?php echo base_url('admin/transaksi/');?>" class="btn btn-light">Kembali</a>
	  	<button type="submit" class="btn btn-primary">Simpan</button>
	  </div>

	</form>
	</article>
</main>




