<main class="col-md-9">
	<div class="card">
	<article class="card-body">
	<header class="mb-8">
		<h4 class="card-title"><?php echo $jenis;?> Produk</h4>
	</header>
	<!-- <form class="block-register" action="<?php //echo base_url($aksi);?>" method="post" > -->
	<?php echo form_open_multipart($aksi,array('class'=>'block-register'));?>
	  <input type="hidden" name="id" value="<?php echo (isset($detail->id))?$detail->id:''; ?>">

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Pilih Kategori</label>
	    <div class="col">
	      <select name="id_kategori" class="form-control col-md-6" required="">
	      	<option value="">Pilih Kategori</option>
	      	<?php
        	foreach ($kategori as $row) { ?>
				<option value="<?php echo $row->id;?>" <?php echo (@$row->id==@$detail->id_kategori)?'selected':''?> ><?php echo $row->nama_kategori;?></option>
	        <?php
	        	}
	        ?>
	      </select>
	      <span class="error text-danger"><?php echo form_error('nama'); ?></span>
	    </div>
	    
	  </div>

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Pilih Merk</label>
	    <div class="col">
	      <select name="id_merk" class="form-control col-md-6" required="">
	      	<option value="">Pilih Merk</option>
	      	<?php
        	foreach ($merk as $row) { ?>
				<option value="<?php echo $row->id;?>" <?php echo (@$row->id==@$detail->id_merk)?'selected':''?> ><?php echo $row->nama_merk;?></option>
	        <?php
	        	}
	        ?>
	      </select>
	      <span class="error text-danger"><?php echo form_error('nama'); ?></span>
	    </div>
	    
	  </div>


	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Nama Produk</label>
	    <div class="col">
	      <input type="text" name="nama" class="form-control col-md-6" placeholder="" required="" value="<?php echo (isset($detail->nama_produk))?$detail->nama_produk:''; ?>">
	      <span class="error text-danger"><?php echo form_error('nama'); ?></span>
	    </div>
	    
	  </div>


	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Deskripsi</label>
	    <div class="col">
	      <textarea name="deskripsi" class="form-control" required="" placeholder=""><?php echo (isset($detail->deskripsi))?$detail->deskripsi:''; ?></textarea>
	      <span class="error text-danger"><?php echo form_error('deskripsi'); ?></span>
	    </div>
	    
	  </div>

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Harga</label>
	    <div class="col">
	      <input type="text" name="harga" class="numericx form-control col-md-4" placeholder="" required="" value="<?php echo (isset($detail->harga))?$detail->harga:''; ?>">
	      <span class="error text-danger"><?php echo form_error('harga'); ?></span>
	    </div>
	    
	  </div>

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Stok</label>
	    <div class="col">
	      <input type="text" name="stok" class="form-control col-md-2" placeholder="" required="" value="<?php echo (isset($detail->stok))?$detail->stok:''; ?>">
	      <span class="error text-danger"><?php echo form_error('stok'); ?></span>
	    </div>
	    
	  </div>

	  <div class="form-group form-row">
	    <label class="col-md-3 col-form-label">Gambar</label>
	    <?php if (@$detail->gambar_utama != '') { ?>
	    	<img src="<?php echo base_url('assets/images/produk/thumbs/'.$detail->gambar_utama);?>" width="50px">
	    <?php
		} 
	    ?>
	    <div class="col">
	      <input type="file" name="gambar" class="form-control col-md-6">
	      <span class="error text-danger"><?php echo form_error('gambar'); ?></span>
	    </div>
	  </div>

	  <div class="float-right">
	  	<a href="<?php echo base_url('admin/produk');?>" class="btn btn-danger">Batal</a>
	  	<button type="submit" class="btn btn-primary">Simpan</button>
	  </div>

	</form>
	</article>
</main>




