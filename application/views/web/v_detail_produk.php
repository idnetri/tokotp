<!-- ============================ COMPONENT 1 ================================= -->
<div class="card">
	<div class="row no-gutters">
		<aside class="col-md-6">
<article class="gallery-wrap"> 
	<div class="img-big-wrap">
	   <a href="#"><img src="<?php echo base_url('assets/images/produk/'.$detail->gambar_utama)?>"></a>
	</div> <!-- img-big-wrap.// -->
	<div class="thumbs-wrap">

		<?php foreach ($gambar as $key => $row) { ?>
			<a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/images/produk/thumbs/'.$row->gambar)?>"></a>		
		<?php } ?>

	</div> <!-- thumbs-wrap.// -->
</article> <!-- gallery-wrap .end// -->
		</aside>
		<main class="col-md-6 border-left">
<article class="content-body">

<h2 class="title"><?php echo $detail->nama_produk;?></h2>

<div class="rating-wrap my-3">
	<ul class="rating-stars">
		<li style="width:80%" class="stars-active"> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
		<li>
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
	</ul>
	<!-- <small class="label-rating text-muted">132 reviews</small>
	<small class="label-rating text-success"> <i class="fa fa-clipboard-check"></i> 154 orders </small> -->
</div> <!-- rating-wrap.// -->

<div class="mb-3"> 
	<var class="price h4"><?php echo format_idr($detail->harga);?></var> 
	<!-- <span class="text-muted">/per kg</span>  -->
</div> 

<p><?php echo $detail->deskripsi;?></p>

<dl class="row">
  <dt class="col-sm-3">Kategori</dt>
  <dd class="col-sm-9"><?php echo $detail->nama_kategori;?></dd>
</dl>

<hr>
	<div class="form-row">
	
	</div> <!-- row.// -->
	<!-- <a href="#" class="btn  btn-primary"> Buy now </a> -->
	<a href="<?php echo base_url('produk/tambah_ke_keranjang/'.$detail->slug);?>" class="btn  btn-outline-primary"> <span class="text"><?php echo TOMBOL_CART;?></span> <i class="fas fa-shopping-cart"></i>  </a>
</article> <!-- product-info-aside .// -->
		</main> <!-- col.// -->
	</div> <!-- row.// -->
</div> <!-- card.// -->
<!-- ============================ COMPONENT 1 END .// ================================= -->

<section class="section-content padding-y bg">
<div class="container">
<div class="card card-body">
	<div class="row">

		<?php foreach ($produk_sejenis as $key => $row) { ?>

			<div class="col-md-3">
				<figure class="itemside mb-4">
					<div class="aside"><img src="<?php echo base_url('assets/images/produk/'.$row->gambar_utama);?>" class="img-sm"></div>
					<figcaption class="info align-self-center">
						<a href="<?php echo base_url('produk/detail/'.$row->slug);?>" class="title"><?php echo $row->nama_produk;?></a>
						<a href="<?php echo base_url('produk/tambah_ke_keranjang/'.$row->slug);?>" class="btn btn-outline-primary btn-sm"> <?php echo TOMBOL_CART;?>
							<i class="fa fa-shopping-cart"></i> 
						</a>
					</figcaption>
				</figure>
			</div> <!-- col.// -->

		<?php } ?>
		
	</div> <!-- row.// -->
</div> <!-- card.// -->

</div> <!-- container .//  -->
</section>