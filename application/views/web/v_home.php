<!-- ========================= SECTION INTRO ========================= -->
<section class="section-intro padding-y-sm">
<div class="container">

<div class="intro-banner-wrap">
	<img src="<?php echo base_url('assets/');?>images/banners/1.jpg" class="img-fluid rounded">
</div>

</div> <!-- container //  -->
</section>
<!-- ========================= SECTION INTRO END// ========================= -->




<!-- ========================= SECTION FEATURE ========================= -->
<section class="section-content padding-y-sm">
<div class="container">
<article class="card card-body">


<div class="row">
  <div class="col-md-4">  
    <figure class="item-feature">
      <span class="text-primary"><i class="fa fa-2x fa-truck"></i></span>
      <figcaption class="pt-3">
        <h5 class="title">Fast delivery</h5>
        
      </figcaption>
    </figure> <!-- iconbox // -->
  </div><!-- col // -->
  <div class="col-md-4">
    <figure  class="item-feature">
      <span class="text-primary"><i class="fa fa-2x fa-landmark"></i></span>  
      <figcaption class="pt-3">
        <h5 class="title">Creative Strategy</h5>
        
         </p>
      </figcaption>
    </figure> <!-- iconbox // -->
  </div><!-- col // -->
    <div class="col-md-4">
    <figure  class="item-feature">
      <span class="text-primary"><i class="fa fa-2x fa-lock"></i></span>
      <figcaption class="pt-3">
        <h5 class="title">High secured </h5>
        
         </p>
      </figcaption>
    </figure> <!-- iconbox // -->
  </div> <!-- col // -->
</div>
</article>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION FEATURE END// ========================= -->





<section class="section-content">
<div class="container">

<header class="section-heading">
	<h3 class="section-title">Produk Terpopuler</h3>
</header><!-- sect-heading -->

	
<div class="row">
	<?php foreach ($produk_populer as $row) { ?>
		
		<div class="col-md-3">
		<div href="<?php echo base_url('produk/detail/'.$row->slug);?>" class="card card-product-grid">
			<a href="<?php echo base_url('produk/detail/'.$row->slug);?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/produk/').$row->gambar_utama;?>"> </a>
			<figcaption class="info-wrap">
				<a href="<?php echo base_url('produk/detail/'.$row->slug);?>" class="title"><?php echo $row->nama_produk;?></a>
				
				<div class="rating-wrap">
					<ul class="rating-stars">
						<li style="width:80%" class="stars-active"> 
							<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
						</li>
						<li>
							<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 
						</li>
					</ul>
					<!-- <span class="label-rating text-muted"> 34 reviws</span> -->
				</div>
				<div class="price mt-1"><?php echo format_idr($row->harga);?></div> <!-- price-wrap.// -->
			</figcaption>
		</div>
	</div> <!-- col.// -->

	<?php } ?>
</div> <!-- row.// -->

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->



<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content">
<div class="container">

<header class="section-heading">
	<h3 class="section-title">Terbaru</h3>
</header><!-- sect-heading -->

<div class="row">
	<?php foreach ($produk_terbaru as $row) { ?>
		
		<div class="col-md-3">
		<div href="<?php echo base_url('produk/detail/'.$row->slug);?>" class="card card-product-grid">
			<a href="<?php echo base_url('produk/detail/'.$row->slug);?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/produk/').$row->gambar_utama;?>"> </a>
			<figcaption class="info-wrap">
				<a href="#" class="title"><?php echo $row->nama_produk;?></a>
				
				<div class="rating-wrap">
					<ul class="rating-stars">
						<li style="width:80%" class="stars-active"> 
							<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
						</li>
						<li>
							<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> 
						</li>
					</ul>
					<!-- <span class="label-rating text-muted"> 34 reviws</span> -->
				</div>
				<div class="price mt-1"><?php echo format_idr($row->harga);?></div> <!-- price-wrap.// -->
			</figcaption>
		</div>
	</div> <!-- col.// -->

	<?php } ?>
	
</div> <!-- row.// -->

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= SECTION  ========================= -->
<section class="section-name bg padding-y-sm">
<div class="container">
<header class="section-heading">
	<h3 class="section-title">Our Brands</h3>
</header><!-- sect-heading -->

<div class="row">
	<div class="col-md-2 col-6">
		<figure class="box item-logo">
			<a href="#"><img src="<?php echo base_url('assets/');?>images/logos/logo1.png"></a>
			<figcaption class="border-top pt-2">36 Products</figcaption>
		</figure> <!-- item-logo.// -->
	</div> <!-- col.// -->
	<div class="col-md-2  col-6">
		<figure class="box item-logo">
			<a href="#"><img src="<?php echo base_url('assets/');?>images/logos/logo2.png"></a>
			<figcaption class="border-top pt-2">980 Products</figcaption>
		</figure> <!-- item-logo.// -->
	</div> <!-- col.// -->
	<div class="col-md-2  col-6">
		<figure class="box item-logo">
			<a href="#"><img src="<?php echo base_url('assets/');?>images/logos/logo3.png"></a>
			<figcaption class="border-top pt-2">25 Products</figcaption>
		</figure> <!-- item-logo.// -->
	</div> <!-- col.// -->
	<div class="col-md-2  col-6">
		<figure class="box item-logo">
			<a href="#"><img src="<?php echo base_url('assets/');?>images/logos/logo4.png"></a>
			<figcaption class="border-top pt-2">72 Products</figcaption>
		</figure> <!-- item-logo.// -->
	</div> <!-- col.// -->
	<div class="col-md-2  col-6">
		<figure class="box item-logo">
			<a href="#"><img src="<?php echo base_url('assets/');?>images/logos/logo5.png"></a>
			<figcaption class="border-top pt-2">41 Products</figcaption>
		</figure> <!-- item-logo.// -->
	</div> <!-- col.// -->
	<div class="col-md-2  col-6">
		<figure class="box item-logo">
			<a href="#"><img src="<?php echo base_url('assets/');?>images/logos/logo2.png"></a>
			<figcaption class="border-top pt-2">12 Products</figcaption>
		</figure> <!-- item-logo.// -->
	</div> <!-- col.// -->
</div> <!-- row.// -->
</div><!-- container // -->
</section>
<!-- ========================= SECTION  END// ========================= -->
