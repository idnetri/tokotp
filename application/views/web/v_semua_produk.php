<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
<div class="container">

<div class="row">
	<aside class="col-md-3">
		
<div class="card">
	<article class="filter-group">
		<header class="card-header">
			<a href="#" data-toggle="collapse" data-target="#collapse_1" aria-expanded="true" class="">
				<i class="icon-control fa fa-chevron-down"></i>
				<h6 class="title">Kategori</h6>
			</a>
		</header>
		<div class="filter-content collapse show" id="collapse_1" style="">
			<div class="card-body">
				<form class="pb-3">
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Pencarian">
				  <div class="input-group-append">
				    <button class="btn btn-light" type="button"><i class="fa fa-search"></i></button>
				  </div>
				</div>
				</form>
				
				<ul class="list-menu">

				<?php $kat = get_kategori();
		        	foreach ($kat as $row) { ?>
					<li><a href="<?php echo base_url('produk/').'?&j=k&key='.$row->slug;?>"><?php echo $row->nama_kategori;?>  </a></li>      

		        <?php
		        	}
		        ?>

				</ul>

			</div> <!-- card-body.// -->
		</div>
	</article> <!-- filter-group  .// -->
	<article class="filter-group">
		<header class="card-header">
			<a href="#" data-toggle="collapse" data-target="#collapse_2" aria-expanded="true" class="">
				<i class="icon-control fa fa-chevron-down"></i>
				<h6 class="title">Merk </h6>
			</a>
		</header>
		<div class="filter-content collapse show" id="collapse_2" style="">
			<div class="card-body">
				<?php 
		        	foreach ($merk as $row) { ?>
		        		<!-- <?php echo base_url('produk/merk/'.$row->slug);?> -->
					<li><a href="<?php echo base_url('produk/').'?&j=m&key='.$row->slug;?>"><?php echo $row->nama_merk;?>  </a></li>  
					<!-- <label class="custom-control custom-checkbox">
					  <input type="checkbox" checked="" class="custom-control-input">
					  <div class="custom-control-label">Mercedes  
					  	<b class="badge badge-pill badge-light float-right">120</b>  </div>
					</label>      -->

		        <?php
		        	}
		        ?>
	</div> <!-- card-body.// -->
		</div>
	</article> <!-- filter-group .// -->
	
</div> <!-- card.// -->

	</aside> <!-- col.// -->
	<main class="col-md-9">

<header class="border-bottom mb-4 pb-3">
		<div class="form-inline">
			<span class="mr-md-auto"><?php echo (isset($text_judul))?$text_judul:'';?>  </span>
			<select class="mr-2 form-control">
				<option>Latest items</option>
				<option>Trending</option>
				<option>Most Popular</option>
				<option>Cheapest</option>
			</select>
			<div class="btn-group">
				<a href="#" class="btn btn-outline-secondary" data-toggle="tooltip" title="List view"> 
					<i class="fa fa-bars"></i></a>
				<a href="#" class="btn  btn-outline-secondary active" data-toggle="tooltip" title="Grid view"> 
					<i class="fa fa-th"></i></a>
			</div>
		</div>
</header><!-- sect-heading -->

<div class="row">
	<?php foreach ($produk as $key => $row) { ?>
		<div class="col-md-4">
			<figure class="card card-product-grid">
				<div class="img-wrap"> 
					<!-- <span class="badge badge-danger"> NEW </span> -->
					<img src="<?php echo base_url('assets/images/produk/').$row->gambar_utama;?>">
					<a class="btn-overlay" href="<?php echo base_url('produk/detail/'.$row->slug);?>"><i class="fa fa-search-plus"></i> Detail</a>
				</div> <!-- img-wrap.// -->
				<figcaption class="info-wrap">
					<div class="fix-height">
						<a href="#" class="title"><?php echo $row->nama_produk;?></a>
						<div class="price-wrap mt-2">
							<span class="price"><?php echo format_idr($row->harga);?></span>
						</div> <!-- price-wrap.// -->
					</div>
					<a href="<?php echo base_url('produk/tambah_ke_keranjang/'.$row->slug);?>" class="btn btn-block btn-primary"><?php echo TOMBOL_CART;?> </a>	
				</figcaption>
			</figure>
		</div> <!-- col.// -->
	<?php } ?>

</div> <!-- row end.// -->


<nav class="mt-4" aria-label="Page navigation sample">
	<?php echo $pagination;?> 
  <!-- <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul> -->
</nav>


	</main> <!-- col.// -->

</div>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
