<?php 
// print_r_pre($this->session->userdata()); die();
if ($this->session->userdata('role') != '2') { ?>
	<!-- ========================= SECTION CONTENT ========================= -->
	<section class="section-conten padding-y" style="min-height:84vh">

	<!-- ============================ COMPONENT LOGIN   ================================= -->
		<div class="card mx-auto" style="max-width: 380px; margin-top:10px;">
	      <div class="card-body">
	      	<div class="alert alert-warning" role="alert">
			  Untuk Checkout Silakan Login Terlebih Dahulu!
			</div>
	      <h4 class="card-title mb-4">Log In</h4>
	      <?php echo form_open('member/login/?&r=checkout');?>
	      	  <!-- <a href="#" class="btn btn-facebook btn-block mb-2"> <i class="fab fa-facebook-f"></i> &nbsp  Sign in with Facebook</a>
	      	  <a href="#" class="btn btn-google btn-block mb-4"> <i class="fab fa-google"></i> &nbsp  Sign in with Google</a> -->
	          <div class="form-group">
				 <input name="username" class="form-control" placeholder="Username" type="text">
	          </div> <!-- form-group// -->
	          <div class="form-group">
				<input name="pass" class="form-control" placeholder="Password" type="password">
	          </div> <!-- form-group// -->
	          
	          <div class="form-group">
	          	<a href="#" class="float-right">Lupa Password?</a> 
	            <label class="float-left custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" checked=""> <div class="custom-control-label"> Remember </div> </label>
	          </div> <!-- form-group form-check .// -->
	          <div class="form-group">
	              <button type="submit" name="submit" class="btn btn-primary btn-block"> Login  </button>
	          </div> <!-- form-group// -->    
	      </form>
	      </div> <!-- card-body.// -->
	    </div> <!-- card .// -->

	     <p class="text-center mt-4">Belum punya akun? <a href="<?php echo base_url('member/daftar');?>">Daftar</a></p>
	     <br><br>
	<!-- ============================ COMPONENT LOGIN  END.// ================================= -->


	</section>
	<!-- ========================= SECTION CONTENT END// ========================= -->

	
<?php } else { ?>
<section class="section-content padding-y bg">
<div class="container">	

	<div class="row">
		<aside class="col-md-12">
	<!-- ============================ COMPONENT 3  ================================= -->

	<div class="card mb-3">
		<article class="card-body">
			<header class="mb-4">
				<h4 class="card-title">Ringkasan Belanja</h4>
			</header>
				<div class="row">
					<?php foreach ($this->cart->contents() as $key => $items) { ?>
					<div class="col-md-6">
						<figure class="itemside  mb-3">
							<div class="aside"><img src="<?php echo base_url('assets/images/produk/'.$items['image']);?>" class="border img-xs"></div>
							<figcaption class="info">
								<p><?php echo $items['name'];?> </p>
								<span><?php echo $items['qty'].'x '.format_idr($items['price']).' = Total: '.format_idr($items['subtotal']);?> </span>
							</figcaption>
						</figure>
					</div> <!-- col.// -->
				<?php } ?>
				</div> <!-- row.// -->
		</article> <!-- card-body.// -->
		<article class="card-body border-top">

			<dl class="row">
			  <dt class="col-sm-10">Subtotal: <span class="float-right text-muted"><?php echo $this->cart->total_items();?></span></dt>
			  <dd class="col-sm-2 text-right"><strong><?php echo format_idr($this->cart->total());?></strong></dd>

			  <dt class="col-sm-10">Diskon: <span class="float-right text-muted">-</span></dt>
			  <dd class="col-sm-2 text-danger text-right"><strong>0</strong></dd>


			  <dt class="col-sm-10">Total:</dt>
			  <dd class="col-sm-2 text-right"><strong class="h5 text-dark"><?php echo format_idr($this->cart->total());?></strong></dd>
			</dl>

		</article> <!-- card-body.// -->
	</div> <!-- card.// -->
	<!-- ============================ COMPONENT 3  ================================= -->
	</aside>
	</div>

	<div class="row">
	<aside class="col-md-12">
<!-- ============================ COMPONENT FEEDBACK  ================================= -->
	<div class="card">
      <div class="card-body">
      <h4 class="card-title mb-4">Data Pengiriman</h4>
      <?php echo form_open('produk/checkout_proses');?>
        <div class="form-row">
			<div class="col form-group">
				<label>Nama Lengkap</label>
			  	<input type="text" name="nama_penerima" value="<?php echo $data_member[0]->nama_lengkap;?>" class="form-control col-sm-4" placeholder="" required="">
			</div> <!-- form-group end.// -->
			
		</div> <!-- form-row.// -->

		<div class="form-group">
			<label>No HP Penerima</label>
			<input type="text" name="no_hp_penerima" value="<?php echo $data_member[0]->no_hp;?>" class="form-control col-sm-3" placeholder="" maxlength="20" required="">
		</div>

		<div class="form-group">
			<label>Alamat Lengkap Pengiriman</label>
			<textarea name="alamat_pengiriman" class="form-control" rows="3" required=""><?php echo $data_member[0]->alamat_lengkap;?></textarea>
		</div>

			<?php echo $cap['image'];?>
		
		<!-- <div style="margin-top: -10px;"></div> -->
		<div class="form-group">
			<label>Masukan Kode Diatas</label>
			<input type="text" name="captcha" class="form-control col-sm-1" placeholder="" required="">
		</div>

		<div class="text-right">
		  	<button type="submit" class="btn btn-primary">Selesaikan Belanja</button>
		</div>
      </form>
      </div> <!-- card-body.// -->
    </div> <!-- card .// -->
<!-- ============================ COMPONENT FEEDBACK END.// ================================= -->
	</aside>
</div>
</div>
</section>

<?php
	}
?>