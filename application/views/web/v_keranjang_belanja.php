<section class="section-content padding-y">
<div class="container">

	<?php tampilkan_notifikasi(); ?>

<div class="row">
	<main class="col-md-12">


		<div class="card">
<div class="row no-gutters">
	<aside class="col-md-9">

			<?php foreach ($this->cart->contents() as $key => $items) { ?>
				<article class="card-body border-bottom">
					<div class="row">
						<div class="col-md-7">
							<figure class="media">
								<div class="img-wrap mr-3"><img src="<?php echo base_url('assets/images/produk/'.$items['image']);?>" class="border img-sm"></div>
								<figcaption class="media-body">
									<a href="#" class="title h6"><?php echo $items['name'];?> </a><br>
									<span><?php echo $items['qty'].' x '.format_idr($items['price']);?> </span>
									<div class="price-wrap">
										<var class="price"><?php echo format_idr($items['subtotal']);?></var>
									</div>
								</figcaption>
							</figure> 
						</div> <!-- col.// -->
						<div class="col-md-5 text-md-right text-right">
							<form action="<?php echo base_url('produk/edit_keranjang');?>" method="post">
							<input type="hidden" name="rowid" value="<?php echo $items['rowid'];?>">
							<div class="input-group input-spinner">
							  <div class="input-group-prepend">
							    <button class="btn btn-light" type="button" id="button-plus"> + </button>
							  </div>
							  <input type="text" name="qty" class="form-control"  value="<?php echo $items['qty'];?>">
							  <div class="input-group-append">
							    <button class="btn btn-light" type="button" id="button-minus"> − </button>
							  </div>
							</div> <!-- input-group.// -->
							<button type="submit" class="btn btn-light button_edit"><i class="fa fa-edit"></i></button>
							
							<a href="#" data-id="<?php echo $items['rowid'];?>" class="btn btn-light button_hapus"> <i class="fa fa-trash"></i> </a>
							</form>
						</div>
					</div> <!-- row.// -->
			</article> <!-- card-group.// -->
			<?php } ?>
	</aside> <!-- col.// -->
	<aside class="col-md-3 border-left">
		<div class="card-body">
			<dl class="dlist-align">
			  <dt>Total harga:</dt>
			  <dd class="text-right"><?php echo format_idr($this->cart->total());?></dd>
			</dl>
			<dl class="dlist-align">
			  <dt>Diskon:</dt>
			  <dd class="text-right text-danger">- 0</dd>
			</dl>
			<dl class="dlist-align">
			  <dt>Total:</dt>
			  <dd class="text-right text-dark b"><strong><?php echo format_idr($this->cart->total());?></strong></dd>
			</dl>
			<hr>
			<a href="<?php echo base_url('produk/checkout');?>" class="btn btn-primary btn-block"> Checkout </a>
			<a href="<?php echo base_url();?>" class="btn btn-light btn-block">Lanjutkan Belanja</a>
		</div> <!-- card-body.// -->
	</aside> <!-- col.// -->
</div> <!-- row.// -->
</div> <!-- card.// -->

	</main> <!-- col.// -->
</div>

</div> <!-- container .//  -->
</section>

<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- Delete Modal content-->

<div class="modal fade" id="delete_confirmation_modal" role="dialog" style="display: none;">
	<div class="modal-dialog" style="margin-top: 260.5px;">
				<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Apakah Anda Yakin Akan Menghapus data ini?</h5>
			</div>
			<form role="form" method="post" action="<?php echo base_url('produk/hapus_keranjang');?>" id="delete_data">
				<input type="hidden" id="delete_item_id" name="id" value="">
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">Ya</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Tidak</button>
				</div>
			</form>
		</div>

	</div>
</div>