<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
  |--------------------------------------------------------------------------
  | function of path css
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_css')) {

    function css($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_css') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of path javascript
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_js')) {

    function js($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_js') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of path images
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_img')) {

    function image($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_img') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of path uploads
  |--------------------------------------------------------------------------
 */
if (!function_exists('path_upload')) {

    function path_upload($file = '') {
        $CI = & get_instance();
        echo $CI->config->item('path_upload') . $file;
    }

}

/*
  |--------------------------------------------------------------------------
  | function of name system
  |--------------------------------------------------------------------------
 */
if (!function_exists('system_name')) {

    function system_name($descript = '') {
        $CI = & get_instance();
        if ($descript != '') {
            return $CI->config->item('name_system') . ' - ' . $descript;
        }
        return $CI->config->item('name_system');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of company
  |--------------------------------------------------------------------------
 */
if (!function_exists('company')) {

    function company() {
        $CI = & get_instance();
        return $CI->config->item('company');
    }

}
/*
  |--------------------------------------------------------------------------
  | function of print_r_pre
  |--------------------------------------------------------------------------
 */

/**
 * 
 * @param array/object $data data yang akan ditampilkan
 */
if (!function_exists('print_r_pre')) {

    function print_r_pre($data = '', $die = false) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        if ($die)
            die();
    }

}

/*
  |--------------------------------------------------------------------------
  | function of Address
  |--------------------------------------------------------------------------
 */
if (!function_exists('address')) {

    function address() {
        $CI = & get_instance();
        return $CI->config->item('address');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of telepon
  |--------------------------------------------------------------------------
 */
if (!function_exists('telepon')) {

    function telepon() {
        $CI = & get_instance();
        return $CI->config->item('telepon');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of version
  |--------------------------------------------------------------------------
 */
if (!function_exists('version')) {

    function version() {
        $CI = & get_instance();
        return $CI->config->item('version');
    }

}


/*
  |--------------------------------------------------------------------------
  | function of developer
  |--------------------------------------------------------------------------
 */
if (!function_exists('developer')) {

    function developer() {
        $CI = & get_instance();
        return $CI->config->item('developer');
    }

}

/*
  |--------------------------------------------------------------------------
  | function of telepon
  |--------------------------------------------------------------------------
 */
if (!function_exists('telepon')) {

    function telepon() {
        $CI = & get_instance();
        return $CI->config->item('telepon');
    }

}

/**
 * -----------------------------------------------------------------------------
 * fungsi filter data
 * -----------------------------------------------------------------------------
 */
if (!function_exists('filter_data')) {

    function filter_data($data) {
        $data = trim($data);
        $back = strtoupper(stripslashes(strip_tags($data, ENT_QUOTES)));
        return $back;
    }

}

if (!function_exists('filter_numeric')) {

    function filter_numeric($no = '') {
        $no = str_replace(',', '', $no);
        $nomor = filter_data($no);
        if ($no == '') {
            return 0;
        }
        return $nomor;
    }

}

if (!function_exists('sentence_case')) {

    function sentence_case($string) {
        $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $new_string = '';
        foreach ($sentences as $key => $sentence) {
            $new_string .= ($key & 1) == 0 ?
                    ucfirst(strtolower(trim($sentence))) :
                    $sentence . ' ';
        }
        return trim($new_string);
    }

}

if (!function_exists('random_string')) {

    function random_string($str = "") {
        $passEnkrip = md5($str);
        return $passEnkrip;
    }

}

if (!function_exists("format_idr")) {

    function format_idr($angka = '', $format = false) {
        $format = $format ? ',00' : '';
        if (strlen($angka) > 0) {
            return 'IDR '.number_format($angka, 0, ',', '.') . $format;
        } else {
            return '0' . $format;
        }
    }

}

if (!function_exists("format_idr_minus")) {

    function format_idr_minus($angka = '', $format = false) {
        $format = $format ? ',00' : '';
        if (strlen($angka) > 0) {
            if (substr(strval($angka), 0, 1) == "-") {
                return '(' . number_format(abs($angka), 0, ',', '.') . $format . ')';
            } else {
                return number_format($angka, 0, ',', '.') . $format;
            }
        } else {
            return '0' . $format;
        }
    }

}

if (!function_exists("format_angka")) {

    function format_angka($angka) {
        if (strlen($angka) > 0) {
            return number_format($angka, 0, ',', '.');
        } else {
            return '0';
        }
    }

}

if (!function_exists('enkripsi_numeric')) {

    function enkripsi_numeric($string = '') {
        $string = filter_numeric($string);
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, ENCRYPTION_KEY, $string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

}

if (!function_exists('enkripsi')) {

    function enkripsi($id) {
        $CI = & get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->encode($id);
    }

}

if (!function_exists('dekripsi')) {

    function dekripsi($id) {
        $CI = & get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->decode($id);
    }

}

if (!function_exists('dekripsi_numeric')) {

    function dekripsi_numeric($string = '') {
        $string = filter_numeric($string);
        if (strlen($string) > 1) {
            return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, ENCRYPTION_KEY, base64_decode($string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
        } else {
            return $string;
        }
    }

}

if (!function_exists('gender')) {

    function gender($type = '') {
        if ($type == '') {
            $data = array(
                '' => '-- Pilih --',
                'L' => 'Laki - Laki',
                'P' => 'Perempuan'
            );
        } else {
            switch ($type) {
                case 'L' : $data = 'Laki - Laki';
                    break;
                case 'P' : $data = 'Perempuan';
                    break;
                default : $data = "";
            }
        }
        return $data;
    }

}

if (!function_exists('kebangsaan')) {

    function kebangsaan($type = '') {
        if ($type == '') {
            $data = array(
                '' => '-- Pilih --',
                'WNI' => 'Indonesia',
                'WNA' => 'Orang Asing'
            );
        } else {
            switch ($type) {
                case 'WNI' : $data = 'Indonesia';
                    break;
                case 'WNA' : $data = 'Orang Asing';
                    break;
                default : $data = "";
            }
        }
        return $data;
    }

}

if (!function_exists('pendidikan')) {

    function pendidikan($type = '') {
        $data = array(
            '' => '-- Pilih --',
            'SD' => 'SD',
            'SMP' => 'SMP',
            'SMA' => 'SMA',
            'SMK' => 'SMK',
            'D I' => 'D I',
            'D II' => 'D II',
            'D III' => 'D III',
            'S1' => 'S1',
            'S2' => 'S2',
            'S3' => 'S3'
        );
        return $data;
    }

}

if (!function_exists("gol_darah")) {

    function gol_darah() {
        $data = array(
            '' => '-- Pilih --',
            'O' => 'Gol. O',
            'A' => 'Gol. A',
            'B' => 'Gol. B',
            'AB' => 'Gol. AB'
        );
        return $data;
    }

}

if (!function_exists("marital")) {

    function marital($kode = '') {
        if ($kode == '') {
            $data = array(
                '' => '-- Pilih --',
                'KW' => 'Kawin',
                'BK' => 'Belum Kawin',
                'JN' => 'Janda',
                'DD' => 'Duda'
            );
        } else {
            switch ($kode) {
                case 'KW' : $data = 'Kawin';
                    break;
                case 'BK' : $data = 'Belum Kawin';
                    break;
                case 'JN' : $data = 'Janda';
                    break;
                case 'DD' : $data = 'Duda';
                    break;
                default : $data = "";
            }
        }
        return $data;
    }

}

if (!function_exists("agama")) {

    function agama() {
        $data = array(
            '' => '-- Pilih --',
            'ISLAM' => 'Islam',
            'KRISTEN' => 'kristen',
            'KATHOLIK' => 'Katholik',
            'HINDU' => 'Hindu',
            'BUDHA' => 'Budha'
        );
        return $data;
    }

}

if (!function_exists('dateToIndo')) {

    function dateToIndo($date = '') {
        $pecah = explode('-', $date);
        if (count($pecah) > 2) {
            $getDate = $pecah[2] . '-' . $pecah[1] . '-' . $pecah[0];
        } else {
            $getDate = $date;
        }
        return $getDate;
    }

}

if (!function_exists("data_404")) {

    function data_404() {
        echo "<h2>Data Not Found</h2>";
    }

}

if (!function_exists('date_differ')) {

    function date_differ($data = array()) {
        if (is_array($data)) {
            $date_diff = (strtotime($data[0]) - strtotime($data[1])) / 86400;
        } else {
            $date_diff = "0";
        }
        return $date_diff;
    }

}

if (!function_exists('session')) {

    function session($name) {
        $CI = & get_instance();
        return $CI->session->userdata($name);
    }

}





if (!function_exists('generate_barcode')) {

    function generate_barcode($kode) {
        $CI = &get_instance();
        $CI->load->helper('url');
        $parm = str_replace("/", "-", $kode);
        $barcode = site_url('barcode/set_barcode/' . $parm);
        return $barcode;
    }

}

if (!function_exists('generate_pdf')) {

    function generate_pdf($view, $options = false) {
        $CI = &get_instance();
        $CI->load->helper('url');
        $CI->load->library('Dompdf_gen');
        $nama = str_replace("_", " ", $CI->uri->segment(2));
        $ori = str_replace("print", "Pdf", $nama);
        $CI->dompdf->load_html($view);
        $CI->dompdf->set_paper('a4', 'portrait');
        $CI->dompdf->render();
        $CI->dompdf->stream(ucfirst($ori) . ".pdf");
    }

}



if (!function_exists('get_status_input')) {

    function get_status_input() {
        $CI = & get_instance();
        $CI->load->database();
        $sql = "SELECT value FROM config WHERE config_name = 'status_input';";
        return $CI->db->query($sql)->row()->value;
    }
}

if (!function_exists('cek_nidn')) {

    function cek_nidn($nidn) {
        $CI = & get_instance();
        $CI->load->database();
        $sql = $CI->db->query("
           SELECT * FROM dosen WHERE nidn= '$nidn'
        ");

        if ($sql->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_user')) {

    function cek_user($p1, $sesi, $redirect = false) {
        $out = false;
        if (is_array($p1)) {
            if (in_array($sesi, $p1)) {
                $out = true;
            }
        } else {
            if ($p1 == $sesi) {
                $out = true;
            }
        }

        if ($out == false) {
            if ($redirect) {
                redirect($redirect);
            } else {
                return $out;
            }
        } else {
            return $out;
        }
    }

}

if (!function_exists('get_id_baru')) {

    function get_id_baru($table, $id) {

        $CI = & get_instance();
        $CI->load->database();
        $sql = "SELECT COALESCE (MAX($id), 0) + 1 AS id_baru FROM $table";

        return $CI->db->query($sql)->row()->id_baru;
    }

}

if (!function_exists('get_config')) {

    function get_config($config_name) {
        $CI = & get_instance();
        $CI->load->database();
        $sql = "SELECT * FROM config WHERE config_name = $config_name;";
        return $CI->db->query($sql)->result();
    }
}

if (!function_exists('get_kategori')) {

    function get_kategori() {
        $CI = & get_instance();
        $CI->load->database();
        $sql = "SELECT * FROM kategori order by id";
        return $CI->db->query($sql)->result();
    }
}


if (!function_exists('is_active_user')) {

    function is_active_user() {

       if ($_SESSION["is_active_user"] == 1 ) {
            return true;
       } else { 
            return false;
       }
    }
}

if (!function_exists('slug')) {
    function slug($text)
    {
       // replace non letter or digits by -
       $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
 
      // trim
       $text = trim($text, '-');
 
      // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
 
      // lowercase
        $text = strtolower($text);
 
      // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
 
      if (empty($text))
      {
       return 'n-a';
       }
 
       return $text;
  }   
  
}

if (!function_exists('cleanstring')) {
    function cleanstring($string) {
       $string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
    }
}

/*
  |--------------------------------------------------------------------------
  | function of buat notifikasi
  |--------------------------------------------------------------------------
 */

/**
 * 
 * @param nama class, judul, text.
 */
if (!function_exists('tampilkan_notifikasi')) {

    function tampilkan_notifikasi() {
        $text = '';
        if(isset($_SESSION['sukses'])){
            $text = '<div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Berhasil!</strong> '.$_SESSION['sukses'].'
                    </div>';


        } elseif (isset($_SESSION['gagal'])) {
             $text = '<div class="alert alert-warning">
                      <strong>Gagal!</strong> '.$_SESSION['gagal'].'
                    </div>';
        }

        echo $text;

    }

}



