-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 25, 2021 at 06:50 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokotp`
--

-- --------------------------------------------------------

--
-- Table structure for table `gambar`
--

CREATE TABLE `gambar` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `judul` text NOT NULL,
  `isi_halaman` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `deskripsi`, `gambar`, `slug`) VALUES
(1, 'Kemeja Pria', 'Kemeja Pria', 'kemeja-pria.jpg', 'kemeja-pria'),
(2, 'Sepatu', 'Sepatu', 'sepatu.jpg', 'sepatu'),
(3, 'Kaos', 'kaos', 'kaos.jpg', 'kaos'),
(6, 'Software', 'Produk software', '', 'software');

-- --------------------------------------------------------

--
-- Table structure for table `merk`
--

CREATE TABLE `merk` (
  `id` int(11) NOT NULL,
  `nama_merk` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merk`
--

INSERT INTO `merk` (`id`, `nama_merk`, `deskripsi`, `slug`, `date_added`) VALUES
(1, 'Microsoft', 'Microsoft', 'microsoft', '2020-01-04 11:10:33'),
(2, 'Dell', 'Dell', 'dell', '2020-01-04 11:10:33'),
(3, 'Oracle', 'Oracle', 'oracle', '2020-01-04 11:11:31'),
(4, 'Amazon', 'Amazon', 'amazon', '2020-01-04 11:11:31'),
(5, 'Toshiba', 'Toshiba', 'toshiba', '2020-01-04 11:12:35'),
(6, 'Asus', 'Asus', 'asus', '2020-01-04 11:12:35'),
(12, 'Apple', 'Deskripsi', 'apple', '2020-04-01 05:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `isi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `alamat_lengkap` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_divalidasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `username`, `password`, `nama_lengkap`, `jenis_kelamin`, `no_hp`, `alamat_lengkap`, `email`, `role`, `image`, `tanggal_register`, `is_divalidasi`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'l', '08572222000', 'Jakarta', 'admin@aaa.com', 1, 'admin.jpg', '2020-02-10 08:14:56', 1),
(2, 'member', 'aa08769cdcb26674c6706093503ff0a3', 'Andi', 'l', '08123456789', 'Sleman, DIY', 'member@aaa.com', 2, 'aaa.jpg', '2021-01-25 05:48:41', 1),
(3, 'jonox', 'e10adc3949ba59abbe56e057f20f883e', 'Jono', 'l', '0877', 'fsdfsd dfsdf dsfdsf sdfdsfsd sdfdsfds dsfdsfsdf', 'aaaa@aaaa.com', 2, '', '2020-09-11 05:51:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `kode_produk` varchar(255) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_merk` int(11) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `gambar_utama` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `dilihat` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `kode_produk`, `id_kategori`, `id_merk`, `nama_produk`, `deskripsi`, `stok`, `harga`, `gambar_utama`, `slug`, `dilihat`, `date_added`) VALUES
(1, 'A0001', 3, 1, 'Kaos Warna Biru Muda', 'Kaos Warna Biru Muda', 5, '30000', 'kaos-warna-biru-muda_1.jpg', 'kaos-warna-biru-muda-1', 0, '2020-01-04 23:22:21'),
(2, 'A0002', 3, 1, 'Kaos Warna Kuning', 'Kaos Warna Kuning', 10, '27000', 'kaos-warna-kuning_1.jpg', 'kaos-warna-kuning_2', 5, '2020-01-04 23:23:27'),
(3, 'A0003', 2, 1, 'Sepatu Gokil Banget ', 'Sepatu Gokil Banget ', 3, '40000', 'sepatu-gokil-banget_1.jpg', 'sepatu-gokil-banget-3', 0, '2020-01-04 23:24:42'),
(4, 'A0004', 2, 2, 'Sepatu Kece Abis', 'Sepatu Kece Abis', 2, '1000000', 'sepatu-kece-abis_1.jpg', 'sepatu-kece-abis-4', 3, '2020-01-04 23:25:24'),
(5, 'A0005', 1, 2, 'Kemeja Abu abu', 'Kemeja abu abu', 2, '45000', 'kemeja-abu-abu_1.jpg', 'kemeja-abu-abu-5', 1, '2020-01-04 23:26:01'),
(6, 'A0006', 1, 2, 'Kemeja Merah', 'Kemeja Merah', 1, '100000000', 'kemeja-merah_1.jpg', 'kemeja-merah-6', 2, '2020-01-04 23:28:41'),
(7, 'A0007', 2, 2, 'Sepatu Merk Adidas', 'Sepatu Merk Adidas', 3, '1000000', 'sepatu-merk-adidas_1.jpg', 'sepatu-merk-adidas-7', 0, '2020-01-04 23:29:46'),
(20, '', 6, 1, 'Windows 10 Advanced', '-', 100, '3000000', 'windows-10-advanced.jpg', 'windows-10-advanced', 0, '2020-04-02 07:25:56'),
(21, '', 6, 1, 'Windows 10 Keren', '-', 10, '2500000', 'windows-10-keren.jpg', 'windows-10-keren', 0, '2020-04-09 10:17:14'),
(22, '', 3, 6, 'Kaos Tobat', '-', 5, '50000', 'kaos-tobat.jpeg', 'kaos-tobat', 0, '2020-04-09 10:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `status_transaksi`
--

CREATE TABLE `status_transaksi` (
  `id` int(11) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_transaksi`
--

INSERT INTO `status_transaksi` (`id`, `status`) VALUES
(1, 'baru'),
(2, 'menunggu pembayaran'),
(3, 'dikirim'),
(4, 'selesai'),
(5, 'dibatalkan');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `nomor_transaksi` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  `tanggal_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL,
  `no_hp_penerima` varchar(20) NOT NULL,
  `alamat_pengiriman` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `nomor_transaksi`, `username`, `id_pengguna`, `tanggal_transaksi`, `total`, `status`, `nama_penerima`, `no_hp_penerima`, `alamat_pengiriman`) VALUES
(11, '20200211-11', 'member', 0, '2021-01-25 05:47:37', 100000000, 'selesai', 'Andi', '08123456789', 'Banjarnegara, Jawa Tengah'),
(12, '20200211-12', 'member', 0, '2021-01-25 05:48:01', 1000000, 'baru', 'Andi', '08123456789', 'Banjarnegara, Jawa Tengah');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah_beli` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`id`, `id_transaksi`, `id_produk`, `jumlah_beli`, `harga`) VALUES
(1, 1, 5, 1, 45000),
(2, 1, 4, 1, 1000000),
(3, 2, 1, 2, 30000),
(4, 3, 6, 1, 100000000),
(5, 4, 4, 1, 1000000),
(6, 4, 5, 1, 45000),
(7, 5, 5, 2, 45000),
(8, 5, 6, 2, 100000000),
(9, 5, 4, 1, 1000000),
(10, 6, 6, 1, 100000000),
(11, 7, 3, 1, 40000),
(12, 8, 1, 1, 30000),
(13, 9, 7, 1, 1000000),
(14, 10, 4, 1, 1000000),
(15, 10, 5, 1, 45000),
(16, 11, 6, 1, 100000000),
(17, 12, 7, 1, 1000000),
(18, 13, 7, 1, 1000000),
(19, 14, 23, 1, 150000),
(20, 15, 3, 1, 40000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merk`
--
ALTER TABLE `merk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `merk`
--
ALTER TABLE `merk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `status_transaksi`
--
ALTER TABLE `status_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
